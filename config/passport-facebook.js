var FacebookStrategy = require('passport-facebook').Strategy;

var User = require('../models/user');
var config = require('../../config');

module.exports = function(passport) {

    passport.use(new FacebookStrategy({
        clientID: config.facebook.app_id,
        clientSecret: config.facebook.app_secret,
        callbackURL: config.facebook.callback,
        profileFields:['id', 'displayName', 'emails', 'picture.type(large)'],
        passReqToCallback: true

        }, function(req, accessToken, refreshToken, profile, done) {

            console.log('Facebook auth profile:');
            console.log(profile);

            passport.findOrCreateUser(
                req,
                (profile.emails && profile.emails.length > 0) ? profile.emails[0].value : '',
                'facebook',
                profile.id,
                profile.displayName,
                profile.displayName,
                profile.photos[0].value,
                (err, user) => {
                    done(null, user);
                }
            );
        }
    ));
}
