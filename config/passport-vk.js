var VKontakteStrategy = require('passport-vkontakte').Strategy;

var User = require('../models/user');
var config = require('../../config');

module.exports = function(passport) {

    passport.use(new VKontakteStrategy({
        clientID: config.vk.app_id,
        clientSecret: config.vk.app_secret,
        callbackURL: config.vk.callback,
        profileFields:['photo_200'],
        lang: 'ru',
        passReqToCallback: true

        }, function(req, accessToken, refreshToken, params, profile, done) {

            console.log('VK auth params:');
            console.log(params);

            console.log('VK auth profile:');
            console.log(profile);

            passport.findOrCreateUser(
                req,
                params.email || '',
                'vk',
                profile.id,
                profile.displayName, 
                profile.displayName, 
                profile.photos[1].value,
                (err, user) => {
                    console.log('returning user:');
                    console.log(user);
                    done(null, user);
                }
            );
        }
    ));
}
