var LocalStrategy = require('passport-local').Strategy;

var async = require('async');

var User = require('../models/user');
var config = require('../../config');
var mailer = require('../modules/mailer');

module.exports = function(passport) {

    // Проверить username / email и пароль, выдать ошибку, если нет такого юзера
    passport.use('local-login', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true
    },
    function(req, username, password, callback) {

        console.log("Processing local login with username: ", username);

        process.nextTick(function() {
            
            console.log("Check if the user is already created");

            User.findOne({ $or: [ 
                { name: username },
                { email: username }
            ]}, function(err, user) {
                if (err) {
                    return callback(err);
                }

                if (!user) {
                	console.log("User with such username is not found");
                    return callback(null, false, { message: 'Неверное имя или пароль.' });
                }

                if (!user.validPassword(password)) {
                	console.log("User found but the password is incorrect");
                    return callback(null, false, { message: 'Неверное имя или пароль.' });

                } else {
                    return callback(null, user);
                }
            });
        });
    }));

	passport.use('local-signup', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback: true
    },
    function(req, email, password, callback) {

        process.nextTick(function() {

        	console.log("Processing local signup for email: ", email);

            console.log("Check if the user is already logged in");

            if (req.user) {
            	console.log('User should logout before trying to create new account')
                return callback(null, req.user);
            }

            console.log('User is not loged in, check whether the user with email ' + email + ' exists');

            async.waterfall([
                // check name
                function(done) {
                    console.log(`Checking for existing name ${req.body.name}`);

                    User
                        .findOne({ name: req.body.name })
                        .exec(function(err, user) {
                            if (user) {
                                return done('Пользователь с таким именем уже зарегистрирован.');
                            }

                            console.log(`Checking for existing name ${req.body.name} ..not found`);
                            done(null);
                        })
                },
                // check email
                function(done) {
                    console.log(`Checking for existing email ${req.body.email}`);

                    User
                        .findOne({ email: req.body.email })
                        .exec(function(err, user) {
                            if (user) {
                                done('Пользователь с такой почтой уже зарегистрирован.');
                                return;
                            }

                            console.log(`Checking for existing email ${req.body.email} ..not found`);
                            done(null);
                        })
                }, 
                // check inviter
                function(done) {
                    console.log(`Checking for inviter`);

                    var inviterId = req.body.inviter;

                    if (!inviterId) {
                        if (!config.auth || !config.auth.defaultInviter) {
                            console.log('#no inviter');
                            return done(null, null);
                        }

                        console.log('#no inviter, defaulting to ' + config.auth.defaultInviter);
                        inviterId = config.auth.defaultInviter;
                    }

                    User
                        .findById(inviterId)
                        .exec(function(err, inviter) {
                            if (!inviter) {
                                done('Не найден приглашающий пользователь.');
                                return;
                            }

                            console.log('#inviter found: ' + inviter.name);
                            done(null, inviter);
                        })
                },
                // create user
                function(inviter, done) {

                    console.log('#create new user');

                    // create the user
                    var newUser = new User(req.body);

                    newUser.password = newUser.generateHash(password);

                    if (inviter) {
                        newUser.inviter = inviter.id;
                    }

                    console.log('#saving user');
                    newUser.save(function(err, createdUser) {

                        if (!inviter) {
                            return done(err, createdUser);
                        }

                        console.log('#update and notify the inviter');

                        mailer.sendInviteAccepted(inviter.email, 
                            createdUser.name, createdUser.displayName, createdUser.email, createdUser.phone, 
                            function(err) {

                                inviter.invitees.addToSet(createdUser.id);
                                inviter.save(function(err) {
                                    
                                    done(err, createdUser);
                                });
                        })
                    });
                },
                // send reg success email
                function(createdUser, done) {

                    console.log('#send reg success email');

                    mailer.sendRegSuccess(createdUser.email, createdUser.name, createdUser.displayName, function(err) {
                        done(err, createdUser);
                    })

                }], function(err, createdUser) {
                    if (err) {
                        callback(null, false, { message: err });
                        return
                    }
                    
                    console.log('#returning created user');
                    console.log(createdUser);
                    
                    callback(null, createdUser);
                });
		});
    }));
}
