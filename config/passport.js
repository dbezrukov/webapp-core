var _ = require('underscore');
var User = require('../models/user');
var config = require('../../config');

module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {

        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.findOrCreateUser = function(req, email, social, socialId, name, displayName, photo, done) {
        
        const deviceId = req.session.deviceId;
        console.log('Passport: deviceId for auth request is: ' + deviceId);

        var query = email ? { email : email } : { social: social, socialId: socialId };

        User.findOne(query, function(err, user) {
            
            if(!user) {
                console.log('Passport: user not exists');

                app.cloudinary.uploader.upload(photo, function(result) { 
                    
                    var newUser = new User({
                        email: email,
                        name: name,
                        displayName: displayName,
                        photo: result.error ? 'assets/avatar' : result.public_id,
                        devices: deviceId ? [deviceId] : [],
                        social: social,
                        socialId: socialId
                    });

                    newUser.save(function(err, newUser) {
                        if (err) return done(err);
                        done(null, newUser);
                    });
                    
                }, { folder: 'avatars' });

                
            } else {
                console.log('Passport: user exists');

                let modified = false;

                if (user.social !== social) {
                    console.log('social network was changed');
                    user.social = social;
                    user.socialId = socialId;
                    modified = true;
                }

                if (deviceId && user.devices.indexOf(deviceId) === -1) {
                    console.log('registering new device: ' + deviceId);
                    user.devices.push(deviceId);
                    modified = true;
                }

                if (!modified) {
                    done(null, user);
                } else {
                    user.save(function(err, user) {
                        if (err) return done(err);
                        done(null, user);
                    });
                }
            }
        });
    };

    require('./passport-local')(passport);

    if (config.ldap) {
        require('./passport-ldap')(passport);
    }
    
    if (config.vk) {
        require('./passport-vk')(passport);
    }

    if (config.facebook) {
        require('./passport-facebook')(passport);
    }
}
