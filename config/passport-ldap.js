var LdapStrategy = require('passport-ldapauth');

var User = require('../models/user');
var config = require('../../config');

module.exports = function(passport) {

	passport.use('ldapauth', new LdapStrategy({
  			server: {
    			url: config.ldap ? config.ldap.url : '',
    			bindDn: 'cn=admin,dc=okocenter,dc=ru',
    			bindCredentials: 'access1!',
    			searchBase: 'dc=okocenter,dc=ru',
    			searchFilter: '(uid={{username}})'
  			},
  			usernameField : 'username',
        	passwordField : 'password',
        	passReqToCallback : true
		}, 
		function(req, ldapUser, done) {
	        
	        process.nextTick(function() {

	        	if (!ldapUser) {
	        		console.log("User with that LDAP name is not found");
					return done(null, false, { error: 'Пользователь не найден в LDAP.' });
	        	}

	        	console.log("LDAP use found:");
	        	console.log(ldapUser);

	        	// user should logout before trying to create new account
            	if (req.user) {
            		console.log("User should logout before trying to create new account");
                	return done(null, req.user);
            	}

            	// ldapInfo: uid, cn, mail

            	console.log("Check whether a user with given ldapId exists");
            	User.findOne({ 'ldapId' :  ldapUser.uid }, function(err, user) {
                	if (err) {
                    	return done(err);
                	}

                	if (user) {
                		console.log("User exists");
                		return done(null, user);
                	}

                	console.log("User does not exist, checking whether a user with given email is created already");
                	User.findOne({ 'email' :  ldapUser.mail }, function(err, user) {
	                	if (err) {
	                    	return done(err);
	                	}

	                	if (user) {
	                		console.log("User exists, assign the ldapId and return him");

	                		user.ldapId = ldapUser.uid;

	                		user.save(function(err, updatedUser) {
								if (err) {
			                    	return done(err);
			                	}
			                	return done(null, updatedUser);
							})
	                	}

	                	console.log("User with given email is not exist, creating");
						var newUser = new User({
							name: ldapUser.cn,
							email: ldapUser.mail,
							ldapId: ldapUser.uid
						});

						newUser.save(function(err, createdUser) {
							if (err) {
		                        return done(err);
		                    }
							return done(null, createdUser);
		                })
                	})
                })
    		})
		}
	));
}
