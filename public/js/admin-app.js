define([

    'jquery',
    'underscore',
    'marionette',

    'application/application',

    'widgets/navbar/navbar_widget',
    
    'views/admin/invite_pane',
    'views/admin/invites_pane',
    'views/admin/user_pane',
    'views/admin/users_pane',
    'views/admin/group_pane',
    'views/admin/groups_pane',

    'models/invite',
    'models/user',
    'models/group',

    'collections/invites',
    'collections/users',
    'collections/groups',
    'collections/group_members',

    'bootstrap'

], function($, _, Marionette, 

	Application,

	NavbarWidget, 

	InvitePane,
	InvitesPane, 
	UserPane, 
	UsersPane, 
	GroupPane, 
	GroupsPane,

	Invite,
	User,
	Group,

	Invites, 
	Users, 
	Groups,
	GroupMembers) {

	var app = new Application({});

	window.app = app;

    app.on("initialize:after", function() {
	  	$('body').append(app.layout.render().el);
	  	
		var modes = [
	  		'<li>\
				<a class="visible-xs" data-toggle="collapse" data-target=".navbar-collapse" href="/groups">Команды</a>\
				<a class="hidden-xs" href="/groups">Команды</a>\
			</li>',
            '<li>\
                <a class="visible-xs" data-toggle="collapse" data-target=".navbar-collapse" href="/invites">Приглашения</a>\
                <a class="hidden-xs" href="/invites">Приглашения</a>\
            </li>',
            '<li>\
                <a class="visible-xs" data-toggle="collapse" data-target=".navbar-collapse" href="/users">Пользователи</a>\
                <a class="hidden-xs" href="/users">Пользователи</a>\
            </li>'
	  	]

        let accountWidget = '<li class="dropdown" style="display: flex; align-items:  center;">\
                <img class="avatar hidden-xs" src="<%= avatar %>"/>\
                <a class="dropdown-toggle" data-toggle="dropdown">\
                    <%= name %> <span class="caret"></span>\
                </a>\
                <ul class="dropdown-menu user-menu">\
                    <li><a href="/">Главная</a></li>\
                    <li><a href="/logout">Выйти</a></li>\
                </ul>\
            </li>';

        var widgets = [
            _.template(accountWidget)(_.extend(app.account.toJSON(), {
                avatar: app.account.get('photostorage') + app.account.get('photo')
            }))
        ]

	  	app.layout.navbar.show(new NavbarWidget({
	  		model: app.account,
	  		modes: modes,
	  		widgets: widgets
	  	}));
	  	
	  	var route = new Route();

	  	Backbone.history.start({
	  		pushState: true,
	  		root: '/admin'
	  	});
    });

	var Route = Backbone.Marionette.AppRouter.extend({
	  	routes : {
	  		''               : 'goGroups',
	  		'invites'        : 'goInvites',
	  		'invites/:id'    : 'goInvite',
	    	'users'          : 'goUsers',
	    	'users/:id'      : 'goUser',
	    	'groups'         : 'goGroups',
	    	'groups/create'  : 'goGroupCreate',
	    	'groups/:id'     : 'goGroup',
            'groups/:id/invite' : 'goGroupInvite'
	  	},

        goInvites: function() {
	  		var invites = new Invites();
	    	invites.fetch({
				success: function() {
					app.layout.content.show(new InvitesPane({
	    				model: app.account,
                        collection: invites
	    			}));
				}
			});
	  	},

	  	goInvite: function(inviteId) {
	    	var invite = new Invite({
	    		_id: inviteId
	    	});
	    	invite.fetch({
				success: function() {
					app.layout.content.show(new InvitePane({
						model: invite
    				}));
				},
                error: function() {
                    app.navigate('/invites', { trigger: true });
                }
			});
	  	},

        goUsers: function() {
	  		var users = new Users;
	  		users.fetch({
				success: function() {
					app.layout.content.show(new UsersPane({
	    				collection: users
	    			}));
				}
			});
	  	},

	  	goUser: function(userId) {
	    	var user = new User({
	    		_id: userId
	    	});
	    	user.fetch({
				success: function() {
					app.layout.content.show(new UserPane({
						model: user
    				}));
				},
                error: function() {
                    app.navigate('/users', { trigger: true });
                }
			});
	  	},

	  	goGroups: function(){
	  		var groups = new Groups;

	    	groups.fetch({
				success: function() {
					app.layout.content.show(new GroupsPane({
	    				collection: groups
	    			}));
				}
			});
	  	},

	  	goGroupCreate: function(groupId) {
	    	var group = new Group();
	    	
			app.layout.content.show(new GroupPane({
				model: group
			}));
	  	},

        goGroup: function(groupId) {
            var group = new Group({
                _id: groupId
            });
            
            group.fetch({
                success: function() {

                    var members = new GroupMembers([], {
                        group: groupId
                    })
                    
                    members.fetch({
                        success: function() {

                            var invites = new Invites([], {
                                group: groupId
                            })

                            invites.fetch({
                                success: function() {

                                    invites.forEach(function(invite) {
                                        members.push({
                                            name: invite.get('name'),
                                            displayName: invite.get('displayName'),
                                            inviteId: invite.get('_id'),
                                        })
                                    })

                                    app.layout.content.show(new GroupPane({
                                        model: group,
                                        collection: members
                                    }));
                                }
                            });
                        }
                    });
                },
                error: function() {
                    app.navigate('/groups', { trigger: true });
                }
            });
        },

        goGroupInvite: function(groupId) {
            var group = new Group({
                _id: groupId
            });
            
            group.fetch({
                success: function() {
                    var invite = new Invite({
                        group: {
                            _id: groupId,
                            name: group.get('name')
                        }
                    });

                    app.layout.content.show(new InvitePane({
                        model: invite
                    }));
                },
                error: function() {
                    app.navigate('/groups', { trigger: true });
                }
            });
        }
	});

    return app;
});
