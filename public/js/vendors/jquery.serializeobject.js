;(function($){

$.fn.serializeObject = function( options ){
   
	var form = this;
		
    var o = {};
    var a = this.serializeArray();
    
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            
            if (this.value) {
				o[this.name] += ', ' + getValue.call(this);
            }
        } else {
            o[this.name] = getValue.call(this) || '';
        }
    });

    function getValue() {
    	var other = this.value.indexOf('Другое');
    	if (other === -1) {
    		return this.value;
    	} else {
    		var otherValue = form.find('#' + this.name + '-other').val();
    		return otherValue;
    	}
    }
	return o;
};

})( jQuery ); // confine scope
