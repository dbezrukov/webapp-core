var coordEpsg = "EPSG:4326";
var tileEpsg = "EPSG:3857";
var minLon = 38.841763244129972;
var maxLon = 38.900178102369971;
var minLat = 45.590659408942173;
var maxLat = 45.631649686574171;
var tilePixSize = 256;
var minZoomLevel = 0;
var maxZoomLevel = 9;

var startLevel = 12;
var tileExtention = "jpg";

//var cacheDir = 'cache_cropio';
var cacheDir = 'data/ndvi/local/2016-06-05';

//var agricultureOpacity = 1.0;
var agricultureOpacity = 0.5;
