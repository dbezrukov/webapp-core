var currentZoom;
var localLevel;
var levelTileCount;
var levelBitmapSize;

var minXProj, maxXProj;
var minYProj, maxYProj;
var sourceProj, destProj;
var tileResolution;

function readMapParameters()
{
	sourceProj = new Proj4js.Proj(coordEpsg);
	destProj = new Proj4js.Proj(tileEpsg);

	var minxmaxy = new Proj4js.Point(minLon, maxLat);
	Proj4js.transform(sourceProj, destProj, minxmaxy);

	var maxxminy = new Proj4js.Point(maxLon, minLat);
	Proj4js.transform(sourceProj, destProj, maxxminy);

	minXProj = minxmaxy.x;
	maxYProj = minxmaxy.y;
	
	maxXProj = maxxminy.x;
	minYProj = maxxminy.y

	var deltaXProj = maxXProj - minXProj;
	var deltaYProj = maxYProj - minYProj;
	
	//для локальной карты
	var resolutionX = deltaXProj / tilePixSize;
	var resolutionY = deltaYProj / tilePixSize;
	if (resolutionX > resolutionY) {
		tileResolution = resolutionX;
	}
	else {
		tileResolution = resolutionY;
	}
	
	if (deltaXProj > deltaYProj) {
		var addedSpaceY = deltaXProj - deltaYProj;
		maxYProj += addedSpaceY;
	}
	else {
		var addedSpaceX = deltaYProj - deltaXProj;	
		maxXProj += addedSpaceX;
	}	
}

function pix2Geo4326(point_x, point_y)
{
		var xproj = point_x * (maxXProj - minXProj) / levelBitmapSize + minXProj;
		var yproj = (levelBitmapSize - point_y) * (maxYProj - minYProj) / levelBitmapSize + minYProj;
		
		var p = new Proj4js.Point(xproj, yproj);
		Proj4js.transform(destProj, sourceProj, p);

		return new L.LatLng(p.y, p.x); 
}

//карта в мировой сетке 3857
function initMapWeb()
{
		currentZoom = startLevel;
		localLevel = 0;
		levelTileCount = Math.pow(2, localLevel);
		levelBitmapSize = levelTileCount * 256;
		
		readMapParameters();
	
		var ravennaOsmLayer = new L.TileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png');
		var ravennaGoogleLayer = new L.TileLayer('https://khms0.google.com/kh/v=196&x={x}&y={y}&z={z}');

		var my_agriculture = new L.LayerGroup();

		var layerArray = [];
		for (var i = 0; i < 10; i++) {
			layerArray[i] = [];
		}

		var tile_imageUrlJpg = cacheDir + '/0/0/0.' + tileExtention;
		var tile1 = L.imageOverlay(tile_imageUrlJpg, [[minLat, minLon], [maxLat, maxLon]], {opacity: agricultureOpacity});
		(layerArray[localLevel]).push(tile1);
		( (layerArray[localLevel])[(layerArray[localLevel]).length-1]).addTo(my_agriculture);
		
		//map		
		var centerMapPoint = new L.LatLng((minLat+maxLat)/2.0, (minLon+maxLon)/2.0);
		var ravennaMap = L.map('map', {
			center: centerMapPoint, 
			zoom: startLevel,
			minZoom: 0,
			maxZoom: 18,
			crs: L.CRS.EPSG3857,
			layers: [ravennaOsmLayer, ravennaGoogleLayer, my_agriculture]
		});
		ravennaMap.attributionControl.setPrefix('zoom: ' + ravennaMap.getZoom().toString());

		ravennaMap.on('zoomend', function() {
			//remove old tile
			if (localLevel >=0) {
				var icount = (layerArray[localLevel]).length;
				for (var i = 0; i < icount; i++) {
					var del_tile = (layerArray[localLevel]).pop( (layerArray[localLevel])[i] );
					ravennaMap.removeLayer(del_tile);
				}
			}
			//удаление полностью слоя целиком
			//ravennaMap.removeLayer(my_agriculture);
			
			//=======================================================================
			ravennaMap.attributionControl.setPrefix('zoom: ' + ravennaMap.getZoom().toString());
			
			currentZoom = ravennaMap.getZoom();
			localLevel = currentZoom - startLevel;
			levelTileCount = Math.pow(2, localLevel);
			levelBitmapSize = levelTileCount * 256;
			//------------------------------
			//console.log(">> currentZoom="+currentZoom);
			//console.log(">> localLevel="+localLevel);
			//console.log(">> levelTileCount="+levelTileCount);
			//console.log(">> levelBitmapSize="+levelBitmapSize);
			//------------------------------
			
			if (currentZoom < startLevel) return;

			//---добавляю локальные тайлы
			var img_x1, img_x2, 
			img_y;
			for (var x = 0; x < levelTileCount; x++) {
				for (var y = 0; y < levelTileCount; y++) {
					//console.log(">> z="+localLevel + ", x=" + x + ", y=" + y);

					img_x1 = 256*x;	
					img_x2 = img_x1 + 256;		
					//console.log(">> img_x1="+img_x1 + ", img_x2=" + img_x2);

					img_y1 = 256*y;	
					img_y2 = img_y1 + 256;		
					//console.log(">> img_y1="+img_y1 + ", img_y2=" + img_y2);

					//minLat, minLon
					var minlatlon = pix2Geo4326(img_x1, img_y2);				
					//maxLat, maxLon
					var maxlatlon = pix2Geo4326(img_x2, img_y1);				

					//if (x==0 && y == 0) {
					//	//console.log(">> minlatlon.lat="+minlatlon.lat + ", minlatlon.lng=" + minlatlon.lng);
					//	L.marker([minlatlon.lat, minlatlon.lng]).addTo(ravennaMap);
					//	//console.log(">> maxlatlon.lat="+maxlatlon.lat + ", maxlatlon.lng=" + maxlatlon.lng);
					//	L.marker([maxlatlon.lat, maxlatlon.lng]).addTo(ravennaMap);
					//}

					//JPG - PNG
					//tile_imageUrl = cacheDir + '/' + localLevel + '/' + x + '/' + y + '.png';
					//L.imageOverlay(tile_imageUrl, [[minlatlon.lat, minlatlon.lng], [maxlatlon.lat, maxlatlon.lng]], {opacity: agricultureOpacity}).addTo(my_agriculture);

					//JPG - PNG------------------------------------
					tile_imageUrlJpg = cacheDir + '/' + localLevel + '/' + x + '/' + y + '.' + tileExtention;
					var tile2 = L.imageOverlay(tile_imageUrlJpg, [[minlatlon.lat, minlatlon.lng], [maxlatlon.lat, maxlatlon.lng]], {opacity: agricultureOpacity});
					(layerArray[localLevel]).push(tile2);
					( (layerArray[localLevel])[(layerArray[localLevel]).length-1]).addTo(my_agriculture);
					//JPG - PNG------------------------------------
					
				} //y
			} //x
			
		});

		
		//кнопка с переключением	
		var baseLayers = {
			"osm": ravennaOsmLayer,
			"satellite": ravennaGoogleLayer
		};
		var overlays = {
			"agriculture": my_agriculture
		};
		L.control.layers(baseLayers, overlays).addTo(ravennaMap);
		

		
//=================================================================
//проаверочные маркеры
//		var checkMarker = L.marker([45.62333, 38.87421]).addTo(ravennaMap);	
//		var minLonmaxLonMarker = L.marker([maxLat, minLon]).addTo(ravennaMap);	
//		var maxLonminLatMarker = L.marker([minLat, maxLon]).addTo(ravennaMap);	
}
