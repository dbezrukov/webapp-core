define([], function() {

	function getCropioColor(ndvi) {
		if (ndvi < 0.0) return "#05123C";
		if (ndvi < 0.033) return "#FFFFFF";
		if (ndvi < 0.067) return "#C4B7A6";
		if (ndvi < 0.1) return "#B4976D";
		if (ndvi < 0.133) return "#A5844F";
		if (ndvi < 0.167) return "#92723F";
		if (ndvi < 0.2) return "#8C8137";
		if (ndvi < 0.25) return "#93B614";
		if (ndvi < 0.3) return "#73AA04";
		if (ndvi < 0.35) return "#64A203";
		if (ndvi < 0.4) return "#4F9405";
		if (ndvi < 0.45) return "#3D8603";
		if (ndvi < 0.5) return "#1E7407";
		if (ndvi < 0.6) return "#066104";
		if (ndvi < 0.7) return "#064904";
		if (ndvi < 0.8) return "#043905";
		if (ndvi < 0.9) return "#042804";
		return "#031102";
	}

	function getCropioWinterWheatRanges(ndvi) {
		if (ndvi < 0.0) return "#05123C";
		if (ndvi < 0.1) return "#42210E";
		if (ndvi < 0.2) return "#A15023";
		if (ndvi < 0.275) return "#CFAA03";
		if (ndvi < 0.325) return "#FFFF00";
		if (ndvi < 0.375) return "#E6EE05";
		if (ndvi < 0.425) return "#D0E105";
		if (ndvi < 0.475) return "#BAD104";
		if (ndvi < 0.525) return "#A1C203";
		if (ndvi < 0.575) return "#89B103";
		if (ndvi < 0.625) return "#71A100";
		if (ndvi < 0.675) return "#5A8F01";
		if (ndvi < 0.725) return "#438200";
		if (ndvi < 0.775) return "#2A7000";
		if (ndvi < 0.925) return "#054128";
		return "#05331F";
	}

    return {
    	ndviColor: function(ndvi) {
    		if (!$.isNumeric(ndvi)) {
    			return 'transparent';
    		}

    		return getCropioColor(ndvi);
    	}
    }
});
