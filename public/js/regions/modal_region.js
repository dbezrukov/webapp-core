define([
    'jquery',
    'marionette'
    
], function($, Marionette) {

    return Marionette.Region.extend({
        el: "#modal-region",

        constructor: function() {
            Marionette.Region.prototype.constructor.apply(this, arguments);
     
            this.ensureEl();
            this.$el.on('hidden', {region:this}, function(event) {
                event.data.region.close();
            });
        },
     
        onShow: function(view) {
            view.on("close", this.onClose, this);
            this.$el.modal('show');
        },
     
        onClose: function() {
            this.$el.modal('hide');
        }
    });
});
