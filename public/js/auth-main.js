require.config({
    waitSeconds: 30,
    baseUrl: '/js/',
    paths: {
        'jquery': 'bower_components/jquery/jquery.min',
        'jquery.serializeobject': 'vendors/jquery.serializeobject',
        'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap.min',
        'underscore': 'bower_components/underscore/underscore-min',
        'backbone': 'bower_components/backbone/backbone-min',
        'backbone.wreqr': 'bower_components/backbone.wreqr/lib/amd/backbone.wreqr.min',
        'backbone.babysitter': 'bower_components/backbone.babysitter/lib/amd/backbone.babysitter.min',
        'marionette': 'bower_components/marionette/lib/core/amd/backbone.marionette.min',
        'tpl': 'bower_components/requirejs-tpl/tpl',
        'combodate': 'bower_components/combodate/src/combodate',
        'translit':  'bower_components/translit/dist/translit'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'jquery.serializeobject': {
		    deps: [ 'jquery' ],
		    exports: '$.fn.serializeObject'
		},
        'bootstrap': {
            deps: ['jquery'],
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette': {
            deps: ['backbone', 'underscore', 'jquery'],
            exports: "Marionette"
        },
        'underscore': {
            exports: '_'
        },
        'combodate': {
            deps: ['jquery']
        }
    },
    map: {
        '*': {
            'css': 'bower_components/require-css/css.min'
        }
    }
});

require([
    'jquery',
    'underscore',
    'marionette',

    'application/application',

    'widgets/navbar/navbar_widget',

    'views/auth/invite_pane',
    'views/auth/login_pane',
    'views/auth/login_modal',
    'views/auth/reset_pane',
    'views/auth/signup_pane',
    'views/auth/forgot_pane',

    'models/account',
    'models/invite',
    
    'css!bower_components/bootstrap/dist/css/bootstrap.min.css',
    'css!stylesheets/common_style',
    'jquery.serializeobject'
    
 ], function($, _, Marionette, 

 	Application,
	
	NavbarWidget,

	InvitePane,
    LoginPane,
    LoginModal,
	ResetPane,
	SignupPane,
	ForgotPane,
	Account,
    Invite) {

 	var app = new Application({});
	window.app = app;
	
    app.on('initialize:after', function() {
		$('body').append(app.layout.render().el);
	  	
	  	var modes = [
	  	]

	  	var widgets = [
		]

		app.layout.navbar.show(new NavbarWidget({
			model: app.account,
			modes: modes,
			widgets: widgets
		}));
	  	
	  	app.route = new Route();

	  	Backbone.history.start({
	  		pushState: true,
	  		root: '/auth'
	  	});
    });

	// setup routes
	var Route = Backbone.Marionette.AppRouter.extend({
	  	routes: {
	     	''       : 'goLogin',
            'invite/:id' : 'goInvite',
	     	'login'  : 'goLogin',
	     	'signup' : 'goSignup',
	     	'reset'  : 'goReset',
	     	'forgot' : 'goForgot'
	  	},
        goInvite: function(inviteId) {
            var invite = new Invite({
                _id: inviteId
            });
            invite.fetch({
                success: function() {
                    app.layout.content.show(new InvitePane({
                        model: invite
                    }));
                },
                error: function() {
                    app.navigate('/login', { trigger: true });
                }
            });
        },
	  	goLogin: function() {
	    	//app.layout.content.show(new LoginPane({}));
            app.layout.content.show(new LoginModal({
                model: app.account
            }));
	  	},
	  	goSignup: function() {
	    	app.layout.content.show(new SignupPane({}));
	  	},
	  	goReset: function() {
	    	app.layout.content.show(new ResetPane({}));
	  	},
	  	goForgot: function() {
	  		app.layout.content.show(new ForgotPane({}));
	  	},
	});

	$(function() {
        app.account = new Account();
		
		app.account.fetch({
			success: function(){
				app.start({})
			}
	    });
    });
});