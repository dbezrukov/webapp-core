define([
    'underscore',
    'backbone'
    
], function(_, Backbone) {

	return Backbone.Model.extend({
        
        idAttribute: '_id',

        defaults: function() {
            return {
				name: 'username',
                displayName: '',
				isAdmin: false
            }
        }
    });
});
