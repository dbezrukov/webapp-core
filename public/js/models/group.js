define([
    'underscore',
    'backbone'

], function(_, Backbone) {

	return Backbone.Model.extend({

        idAttribute: '_id',
        urlRoot: '/api/groups',

        defaults: function() {
            return {
            	name: 'team',
            	displayName: ''
            }
        }
    });
});