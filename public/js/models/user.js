define([
    'underscore',
    'backbone'
    
], function(_, Backbone) {

	return Backbone.Model.extend({
        
        idAttribute: '_id',
        urlRoot: '/api/users',

        defaults: function() {
            return {
				name: 'username',
                displayName: '',
                department: '',
                position: '',
				email: '',
                social: '',
                socialId: '',
                phone: '',
				photo: '',
            	groups: [],
            	groupsAdmin: []
            }
        }

    });
});
