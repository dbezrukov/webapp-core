define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({

        idAttribute: '_id',
        urlRoot: '/api/invites',

        defaults: function() {
            return {
                email: '',
                displayName: '',
                department: '',
                position: '',
                group: {
                    name: ''
                }
            }
        },

        accept: function(data, callback) {
            var self = this;

            $.ajax({ 
                type: 'POST', 
                url: self.urlRoot + '/' + self.get('_id') + '/accept',
                data: data,
                dataType: 'json' 
            })
            .done(function(response) {
                callback();
            })
            .fail(function(jqXHR, textStatus, err) {
                callback(JSON.parse(jqXHR.responseText).error);
            });
        },

        resendSMS: function(data, callback) {
            var self = this;

            $.ajax({ 
                type: 'POST', 
                url: self.urlRoot + '/' + self.get('_id') + '/resend-sms',
                data: data,
                dataType: 'json' 
            })
            .done(function(response) {
                callback();
            })
            .fail(function(jqXHR, textStatus, err) {
                callback(JSON.parse(jqXHR.responseText).error);
            });
        }
    });
});