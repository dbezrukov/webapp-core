define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({
    	
        idAttribute: 'id',
        url: '/api/account/profile',

        hasGroup: function(group) {
            return _.some(this.get('groups'), (el) => el.name === group);
        },

        invitees: function(callback) {
            var self = this;

            $.ajax({ 
                type: 'GET', 
                url: '/api/account/invitees',
                dataType: 'json'
            })
            .done(function(response) {
                callback(null, response);
            })
            .fail(function(jqXHR, textStatus, err) {
                callback(JSON.parse(jqXHR.responseText).error);
            });
        }
    });
});
