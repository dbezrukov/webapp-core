define([
    'backbone',
    'models/group_member'

], function(Backbone, Model) {

    return Backbone.Collection.extend({
        model: Model,

        initialize: function(models, options) {
            this.group = options.group;
        },

        url: function() { 
            return '/api/groups/' + this.group + '/members'
        }
    });
});
