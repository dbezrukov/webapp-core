define([
    'backbone',
    'models/invite'

], function(Backbone, ItemModel) {
	
    return Backbone.Collection.extend({
        model: ItemModel,

        initialize: function(models, options) {
            this.group = options ? options.group : null;
        },

        url: function() {
            return this.group
                ? '/api/invites?group=' + this.group
                : '/api/invites';
        }
    });
});