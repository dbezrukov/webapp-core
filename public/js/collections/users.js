define([
    'backbone',
    'models/user'

], function(Backbone, ItemModel) {

    return Backbone.Collection.extend({
        
        model: ItemModel,
        url: function() { 
        	return '/api/users';
        }
    });
});
