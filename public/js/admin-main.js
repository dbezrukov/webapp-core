require.config({
    waitSeconds: 30,
    baseUrl: '/js/',

    paths: {
        'jquery': 'bower_components/jquery/jquery.min',
        'jquery.serializeobject': 'vendors/jquery.serializeobject',
        'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap.min',
        'bootstrap-multiselect': 'bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect',
        'bootstrap-treeview': 'vendors/bootstrap-treeview/bootstrap-treeview',
        'underscore': 'bower_components/underscore/underscore-min',
        'backbone': 'bower_components/backbone/backbone-min',
        'backbone.wreqr': 'bower_components/backbone.wreqr/lib/amd/backbone.wreqr.min',
        'backbone.babysitter': 'bower_components/backbone.babysitter/lib/amd/backbone.babysitter.min',
        'marionette': 'bower_components/marionette/lib/core/amd/backbone.marionette.min',
        'tpl': 'bower_components/requirejs-tpl/tpl',
        'x-editable': 'bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min',
        'sprintfjs': 'bower_components/sprintf/dist/sprintf.min',
        'combodate': 'bower_components/combodate/src/combodate'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'jquery.serializeobject': {
		    deps: [ 'jquery' ],
		    exports: '$.fn.serializeObject'
		},
        'bootstrap': {
            deps: ['jquery'],
        },
		'bootstrap-multiselect': {
            deps: ['jquery'],
        },
        'bootstrap-treeview': {
            deps: ['jquery', 'bootstrap']
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette': {
            deps: ['backbone', 'underscore', 'jquery'],
            exports: "Marionette"
        },
        'underscore': {
            exports: '_'
        },
        'x-editable' : {
        	deps: ['bootstrap']
        },
        'combodate': {
            deps: ['jquery']
        }
    },
    map: {
        '*': {
            'css': 'bower_components/require-css/css.min'
        }
    }
});

require([
    'jquery', 
    'admin-app',
    'models/account',

    'css!bower_components/bootstrap/dist/css/bootstrap.min.css',
    'css!stylesheets/common_style',

 ], function($, app, Account) {

    $(function() {

        app.account = new Account();

		app.account.fetch({
			success: function(){
				app.start({})
			}
	    });
    });
});
