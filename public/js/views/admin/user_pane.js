define([
    'jquery',
    'underscore',
    'marionette',

    'tpl!templates/admin/user_pane.tmpl',

    'jquery.serializeobject'

], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'user-pane',

        ui: {
        	'alert': '.alert'
        },

        events: {
            'click a.cancel': 'cancelUser',
            'click a.save': 'saveUser',
            'click a.delete': 'deleteUser'
        },

        templateHelpers: {
            created: function() {
                return !!this._id;
            }
        },

        cancelUser: function() {
            var self = this;

            app.navigate('/users', {
                trigger: true
            });
        },

        saveUser: function() {
        	var self = this;
			
			var data = $(self.el).find('form').serializeObject();

			self.ui.alert.hide();
			
			var options = {
				type: 'POST',
				wait: true,
				patch: true,
			    success: function (model, response) {
					app.navigate('/users', { 
                        trigger: true 
                    });
			    },
			    error: function (model, response) {
					self.ui.alert.text(response.responseJSON 
						? response.responseJSON.error
						: response.responseText);

					self.ui.alert.show();
	            	self.ui.alert.focus();
			    }
			}
			
			if (self.options.collection) {
        		self.options.collection.create(data, options)
        	} else {
        		self.model.save(data, options);
			}
        }, 

		deleteUser: function(){
			var self = this;

			var confirmation = confirm('Удалить этого пользователя?');

		    if (confirmation !== true) {
		        return;
		    }

            self.model.destroy({
                success: function (model, response) {
                    app.navigate('/', { trigger: true });
                },
                error: function (model, response) {
                    self.ui.alert.text(response.responseJSON 
                        ? response.responseJSON.error
                        : response.responseText);

                    self.ui.alert.show();
                    self.ui.alert.focus();
                }
            });
        }
    })
});
