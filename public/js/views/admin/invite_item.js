define([
	'underscore',
    'marionette',
    'tpl!templates/admin/invite_item.tmpl'

], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'invite-item',

        ui: {
        },

        events: {
        },

        templateHelpers: {
            
            /*
            statusHelper: function() {
                var self = this;

                if (!self.emailConfirmed) {
					return 'Ожидается подтверждение почты';
				}
				
				if (!self.phoneConfirmed) {
					return 'Ожидается подтверждение телефона';
				}
					
				return 'Принято';
            }
            */
        }
    });
});
