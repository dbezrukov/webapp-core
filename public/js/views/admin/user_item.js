define([
	'underscore',
    'marionette',
    
    'tpl!templates/admin/user_item.tmpl'
   
], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',

        templateHelpers: {
            helperNumber: function() {
                return ++app.userCounter;
            },
            helperSocial: function() {
                return (this.social + '/' + this.socialId);
            },
            helperSocialLink: function() {
                if (this.social === 'vk') {
                    return 'https://vk.com/id' + this.socialId;
                }
                return '';
            }
        },
    });
});