define([
    'jquery',
    'underscore',
    'marionette',

    'views/admin/group_item',
    'tpl!templates/admin/groups_pane.tmpl'

], function($, _, Marionette, 
	ItemView, template) {

    return Marionette.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '.groups',
        tagName: "div",
        className: "groups-pane",

        ui: {
        },

        events: {
            'click a.create': 'createGroup'
        },

        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

		createGroup: function() {

			var self = this;

			app.navigate('/groups/create', { trigger: true });

		    return false;
		}
    });
});
