define([
	'underscore',
    'marionette',
    
    'tpl!templates/admin/group_item.tmpl'

], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',

        ui: {
        },

        modelEvents: {
        	'change': 'render'
        },

        templateHelpers: {
            dateString: function() {
            	function formatDate(date) {
					return (date.toLocaleDateString() + ' ' + date.toLocaleTimeString());
				}
                return formatDate(new Date(this.utc * 1000));
            }
        },

        initialize: function() {
            var self = this;

            //self.model.on('change:online', self._onOnlineChanged, self);
        }
    });
});