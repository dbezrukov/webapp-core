define([
    'jquery',
    'underscore',
    'marionette',
    'sprintfjs',

    'collections/users',

    'views/admin/member_item',

    'tpl!templates/admin/group_pane.tmpl',
    'css!stylesheets/group_pane',

    'bootstrap-multiselect',
    'css!bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css',

], function($, _, Marionette, sprintf,
	Users,
	ItemView,
	template,
    style) {

    function multiselectData(entities) {
		var data = [];

		data.push({ label: 'Выберите', value: '' });

		entities.forEach(function(entity) {
	    	data.push({ 
	    		label: entity.get('displayName'), 
	    		value: entity.get('_id') 
	    	});
		});

		return data;
	}

    return Marionette.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '.members',
        tagName: 'div',
        className: 'group-pane',

        ui: {
        	alert: '.alert',
        	select : '.multiselect'
        },

        events: {
            'click a.save': 'saveGroup',
            'click a.delete': 'deleteGroup',
            'click a.add': 'addMember',
            'keypress input[name=name]': 'nameKeyPressed',
        },

        templateHelpers: {
  			created: function() {
    			return !!this._id;
  			}
		},

        nameKeyPressed: function(event) {
			if(!/[0-9a-z_]/.test(String.fromCharCode(event.which)))
				return false;
		},

        onRender: function() {
        	var self = this;

    		self.ui.select.multiselect({
                //buttonClass: 'btn btn-link',
	        	nonSelectedText: '',
        		onChange: function(option, checked) {

        			if (checked) {

        				var cropId = option.val();

        				console.log('user if: ' + cropId);
        			}
            	}
            });	

			var allUsers = new Users;
			allUsers.fetch({
				success: function() {
					self.ui.select.multiselect('dataprovider', multiselectData(allUsers));
				}
			});
        },

        saveGroup: function() {
        	var self = this;
			
			var data = $(self.el).find('form').serializeObject();

			self.ui.alert.hide();
			
			var options = {
				type: 'POST',
				wait: true,
				patch: true,
			    success: function (model, response) {
					app.navigate('/groups/' + model.get('_id'), { trigger: true });
			    },
			    error: function (model, response) {
					self.ui.alert.text(response.responseJSON 
						? response.responseJSON.error
						: response.responseText);

					self.ui.alert.show();
	            	self.ui.alert.focus();
			    }
			}
			
			self.model.save(data, options);
        },

		deleteGroup: function(){
			var self = this;

			var confirmation = confirm('Удалить эту группу?');

		    if (confirmation !== true) {
		        return;
		    }

            self.model.destroy({
                success: function (model, response) {
                    app.navigate('/groups', { trigger: true });
                },
                error: function (model, response) {
                    self.ui.alert.text(response.responseJSON 
                        ? response.responseJSON.error
                        : response.responseText);

                    self.ui.alert.show();
                    self.ui.alert.focus();
                }
            });
        },

		addMember: function() {
			var self = this;

			var userId = self.ui.select.val();
			
            if (!userId) {
                console.log('missing userId');
                return;
            }

            var options = {
                type: 'POST',
                wait: true,
                success: function (model, response) {
                    console.log('member created');
                },
                error: function (model, response) {
                    self.ui.alert.text(response.responseJSON 
                        ? response.responseJSON.error
                        : response.responseText);

                    self.ui.alert.show();
                    self.ui.alert.focus();
                }
            }

            self.collection.create({
               userId: userId
            }, options)
		}
    })
});
