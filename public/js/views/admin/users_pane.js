define([
    'jquery',
    'underscore',
    'marionette',

    'views/admin/user_item',

    'tpl!templates/admin/users_pane.tmpl'
    
], function($, _, Marionette, ItemView, template) {

    return Marionette.CompositeView.extend({

        template: template,
        itemView: ItemView,
        itemViewContainer: '.users',
        tagName: 'div',
        className: 'users-pane',

        onBeforeRender: function(){
            app.userCounter = 0;
        }
    });
});
