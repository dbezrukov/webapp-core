define([
    'jquery',
    'underscore',
    'marionette',
    'views/admin/invite_item',
    'tpl!templates/admin/invites_pane.tmpl',
    'bootstrap-treeview',
    
], function($, _, Marionette, 
    ItemView, 
    template) {

    return Marionette.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '.invites',
        tagName: 'div',
        className: 'invites-pane',

        ui: {
            'invitees': '#invitees'
        },

        onRender: function() {
            var self = this;

            self.model.invitees(function(err, invitees) {

                $(self.ui.invitees).treeview({
                    enableLinks: true,
                    data: invitees,
                    hrefHelper: function() {
                        return '/users/' + this._id
                    },
                    textHelper: function() {
                        var text = this.displayName;
                        if (this.department && this.position) {
                            text += sprintf(' (%s, %s)', this.department, this.position);
                        }
                        return text;
                    },
                });
            })
        },
    });
});
