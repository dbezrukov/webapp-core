define([
    'underscore',
    'marionette',

    'models/invite',
    'tpl!templates/admin/invite_pane.tmpl',
    
    'jquery.serializeobject'
        
], function(_, Marionette, Invite, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'invite-pane',

        ui: {
            'alert': '.alert',
        },

        events: {
            'click a.send': 'sendInvite',
            'click a.delete': 'deleteInvite',
        },

        templateHelpers: {
            created: function() {
                return !!this._id;
            },

            status: function() {
                var self = this;

                if (self.userId) {
                    return 'Принято';
                } if (self.emailConfirmationCode) {
                    return 'Письмо отправлено, ожидается регистрация';
                } else {
                    return 'Письмо не отправлено, ошибка';
                }
            }
        },

        onRender: function() {
            var self = this;
        },

        sendInvite: function() {
            var self = this;
            
            var data = $(self.el).find('form').serializeObject();

            self.ui.alert.hide();

            var options = {
                type: 'POST',
                wait: true,
                patch: true,
                success: function (model, response) {
                    app.navigate('/groups/' + model.get('group'), { trigger: true });
                },
                error: function (model, response) {
                    self.ui.alert.text(response.responseJSON 
                        ? response.responseJSON.error
                        : response.responseText);

                    self.ui.alert.show();
                    self.ui.alert.focus();
                }
            }
            
            self.model.save(data, options);
        },

        deleteInvite: function() {
            var self = this;

            var confirmation = confirm('Удалить это приглашение?');

            if (confirmation !== true) {
                return;
            }

            self.model.destroy({
                success: function (model, response) {
                    app.navigate('/', { trigger: true });
                },
                error: function (model, response) {
                    self.ui.alert.text(response.responseJSON 
                        ? response.responseJSON.error
                        : response.responseText);

                    self.ui.alert.show();
                    self.ui.alert.focus();
                }
            });
        },
    });
});
