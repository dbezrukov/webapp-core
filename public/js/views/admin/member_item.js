define([
	'underscore',
    'marionette',
    
    'tpl!templates/admin/member_item.tmpl',
    'bootstrap-multiselect',
    'css!bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css',
   
], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',

        ui: {
        	'select' : '.multiselect'
        },

        events: {
            'click a.make-admin': 'makeAdmin',
            'click a.make-member': 'makeMember',
            'click a.remove': 'removeUser'
        },

        modelEvents: {
            'change': 'render'
        },

        templateHelpers: {
            menu: function() {

                var self = this;

                // an invited member
                if (this.inviteId) {
                    var  htmlTemplate = '<a href="%s" class="dropdown-toggle" data-toggle="dropdown" style="cursor: pointer;">Приглашен в команду';
                    return sprintf(htmlTemplate, '/invites/' + self.inviteId);

                } /*else if (this._id === app.account.get('id')) {
                    var  htmlTemplate = 'Владелец команды';
                    return sprintf(htmlTemplate);

                }*/ else if (self.isAdmin) {
                    return '\
                        <a class="dropdown-toggle" data-toggle="dropdown" style="cursor: pointer;">Администратор команды\
                        <span class="caret"></span></button>\
                        <ul class="dropdown-menu">\
                            <li><a class="make-member" style="cursor: pointer;">Сделать участником</a></li>\
                            <li><a class="remove" style="cursor: pointer;">Убрать</a></li>\
                        </ul>';
                } else {
                    return '\
                        <a class="dropdown-toggle" data-toggle="dropdown" style="cursor: pointer;">Участник команды\
                        <span class="caret"></span></button>\
                        <ul class="dropdown-menu">\
                            <li><a class="make-admin" style="cursor: pointer;">Сделать администратором</a></li>\
                            <li><a class="remove" style="cursor: pointer;">Убрать</a></li>\
                        </ul>';
                }

            }
        },

        onRender: function() {
        	var self = this;
		},

        makeAdmin: function() {
            var self = this;

            console.log('make admin ' + self.model.get('_id'));

            self.model.save({
                isAdmin: true
            }, {
                type: 'POST',
                wait: true,
            });
        },

        makeMember: function() {
            var self = this;

            console.log('make admin ' + self.model.get('_id'));

            self.model.save({
                isAdmin: false
            }, {
                type: 'POST',
                wait: true,
            });
        },

        removeUser: function() {
            var self = this;

            var confirmation = confirm('Удалить участника?');

            if (confirmation !== true) {
                return;
            }

            self.model.destroy();
        }
	})
});
