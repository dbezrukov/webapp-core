define([
    'jquery',
    'underscore',
    'marionette',
    'translit',
    'tpl!templates/auth/invite_pane.tmpl',

], function($, _, Marionette, translit, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'padding-top',

        ui: {
            alert: '.alert',
            name: 'input[name="name"]',
            phone: 'input[name="phone"]',
            phoneConfirmationCode: 'input[name="phoneConfirmationCode"]'
        },

        events: {
            'click a.resend': 'resendSMSClicked',
            'click a.accept': 'acceptClicked',
            'keypress input': 'keyPressed'
        },

        templateHelpers: {
            suggestedName: function() {
                var fio = this.displayName.trim().toLowerCase().split(' ');
                return translit(fio[1])[0] + translit(fio[0]);
            }
        },

        onDomRefresh: function() {
            
            var self = this;
            self.ui.name.focus();
        },

        onRender: function() {

            var self = this;
        },

        keyPressed: function(event) {
            var self = this;

            if (event.keyCode == 13) {
                self.acceptClicked();

                event.preventDefault();
                return false;
            }
        },

        resendSMSClicked: function() {
            var self = this;

            var data = $(self.el).find('form').serializeObject();

            self.model.resendSMS(data, function(err) {
                if (err) {
                    self.ui.alert.text(err);
                    self.ui.alert.show();

                    self.ui.phone.focus();
                    return;
                }

                self.ui.phoneConfirmationCode.focus();
            });
        },

        acceptClicked: function() {
            var self = this;

            var data = $(self.el).find('form').serializeObject();

            function endsWith(str, suffix) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }

            /*
            if (!endsWith(data.name, 'okocenter.ru')) {
                self.ui.alert.text('Выберите почтовый ящик в домене @okocenter.ru.');
                self.ui.alert.show();
                return;
            }
            */

            if (data.password != data.password2) {
                self.ui.alert.text('Пароли не совпадают!');
                self.ui.alert.show();
                return;
            }

            function getQueryVariable(variable) {
               var query = window.location.search.substring(1);
               var vars = query.split("&");
               for (var i=0;i<vars.length;i++) {
                       var pair = vars[i].split("=");
                       if(pair[0] == variable){return pair[1];}
               }
               return;
            }

            var code = getQueryVariable('code');
            if (code) {
                data.emailConfirmationCode = code;
            }

            self.ui.alert.hide();
            
            self.model.accept(data, function(err) {
                if (err) {
                    self.ui.alert.text(err);
                    self.ui.alert.show();

                    self.ui.name.focus();
                    return;
                }

                window.location.href = '/';
            });
        }
    })
});
