define([
    'jquery',
    'underscore',
    'marionette',
    'vendors/getqueryvariable',
    'tpl!templates/auth/login_pane.tmpl',
    'css!bower_components/font-awesome/css/font-awesome.min.css',
    'jquery.serializeobject'

], function($, _, Marionette, getQueryVariable, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'padding-top',

        /*
        ui: {
			alert: '.alert',
			username: 'input[name="username"]'
        },

        events: {
        	'click a.login': 'loginClicked',
            'click a.login-vk': 'loginVkClicked',
            'click a.login-facebook': 'loginFacebookClicked',
        	'keypress input': 'keyPressed'
        },

        onDomRefresh: function() {
  			var self = this;
        	self.ui.username.focus();
		},

		keyPressed: function(event) {
        	var self = this;

        	if (event.keyCode == 13) {
        		self.loginClicked();

        		event.preventDefault();
        		return false;
        	}
        },

		loginClicked: function() {
			var self = this;

			var data = $(self.el).find('form').serializeObject();

			self.ui.alert.hide();

            $.ajax({ 
		        type: 'POST', 
	        	url: '/api/auth/login/local',
		        data: data,
		        dataType: 'json' 
		    })
		        .done(function( response ) {
					window.location.href = getQueryVariable('return_to') || '/';
		        })
		        .fail(function(jqXHR, textStatus, err) {
		            self.ui.alert.text(JSON.parse(jqXHR.responseText).error);
		            self.ui.alert.show();

					self.ui.username.focus();
		        });
		},

        loginVkClicked: function() {
            window.location.href = '/api/auth/vk' + window.location.search; // pass return-to
        },

        loginFacebookClicked: function() {
            window.location.href = '/api/auth/facebook' + window.location.search;
        }
        */
    })
});
