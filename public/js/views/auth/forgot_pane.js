define([
    'jquery',
    'underscore',
    'marionette',
    'tpl!templates/auth/forgot_pane.tmpl'

], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'padding-top',

        ui: {
			alertDanger: '.alert-danger',
			alertSuccess: '.alert-success',
			email: 'input[name="email"]'
        },

        events: {
        	'click a.reset': 'resetClicked',
        	'keypress input': 'keyPressed'
        },

        onDomRefresh: function() {
  			
  			var self = this;
        	self.ui.email.focus();
		},

		keyPressed: function(event) {
        	var self = this;

        	if (event.keyCode == 13) {
        		self.resetClicked();

        		event.preventDefault();
        		return false;
        	}
        },

		resetClicked: function() {
			var self = this;

			var data = $(self.el).find('form').serializeObject();

			self.ui.alertDanger.hide();
			self.ui.alertSuccess.hide();

			$.ajax({ 
		        type: 'POST', 
	        	url: '/api/auth/forgot',
		        data: data,
		        dataType: 'json' 
		    })
		        .done(function( response ) {
		            self.ui.alertSuccess.text('Ссылка для сброса пароля отправлена на вашу почту.');
		            self.ui.alertSuccess.show();
		        })
		        .fail(function(jqXHR, textStatus, err) {
		            self.ui.alertDanger.text(JSON.parse(jqXHR.responseText).error);
		            self.ui.alertDanger.show();

					self.ui.email.focus();
		        });
		}
    })
});
