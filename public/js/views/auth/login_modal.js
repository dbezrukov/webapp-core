define([
    'jquery',
    'underscore',
    'marionette',
    'vendors/getqueryvariable',
    'tpl!templates/auth/login_modal.tmpl',
    'css!bower_components/font-awesome/css/font-awesome.min.css',
    'jquery.serializeobject'

], function($, _, Marionette, getQueryVariable, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'padded-wrapper login-modal col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4',

        ui: {
            alert: '.alert',
            username: 'input[name="username"]'
        },

        events: {
            'click a.login': 'loginClicked',
            'click a.login-vk': 'loginVkClicked',
            'click a.login-facebook': 'loginFacebookClicked',
            'keypress input': 'keyPressed'
        },

        templateHelpers: {
            helperHasClose: function() {
                return (window.location.pathname != '/auth/login');
            }
        },

        onDomRefresh: function() {
            var self = this;
            self.ui.username.focus();
        },

        keyPressed: function(event) {
            var self = this;

            if (event.keyCode == 13) {
                self.loginClicked();

                event.preventDefault();
                return false;
            }
        },

        loginClicked: function() {
            var self = this;

            var data = $(self.el).find('form').serializeObject();

            self.ui.alert.hide();

            $.ajax({ 
                type: 'POST', 
                url: '/api/auth/login/local',
                data: data,
                dataType: 'json' 
            })
                .done(function( response ) {
                    if (window.location.pathname === '/auth/login') {
                        window.location.href = '/';
                    } else {
                        window.location.reload();
                    }
                })
                .fail(function(jqXHR, textStatus, err) {
                    self.ui.alert.text(JSON.parse(jqXHR.responseText).error);
                    self.ui.alert.show();

                    self.ui.username.focus();
                });
        },

        loginVkClicked: function() {
            var return_to = window.location.pathname === '/auth/login' ? '/' : window.location.pathname;
            window.location.href = '/api/auth/vk?return_to=' + return_to;
        },

        loginFacebookClicked: function() {
            var return_to = window.location.pathname === '/auth/login' ? '/' : window.location.pathname;
            window.location.href = '/api/auth/facebook?return_to=' + return_to;
        }
    })
});
