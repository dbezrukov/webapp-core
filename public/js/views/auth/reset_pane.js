define([
    'jquery',
    'underscore',
    'marionette',
    'tpl!templates/auth/reset_pane.tmpl'

], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'padding-top',

        ui: {
			alert: '.alert',
			password: 'input[name="password"]'
        },

        events: {
        	'click a.reset': 'resetClicked',
        	'keypress input': 'keyPressed'
        },

        onDomRefresh: function() {
  			
  			var self = this;
        	self.ui.password.focus();
		},

		keyPressed: function(event) {
        	var self = this;

        	if (event.keyCode == 13) {
        		self.resetClicked();

        		event.preventDefault();
        		return false;
        	}
        },

		resetClicked: function() {
			var self = this;

			var data = $(self.el).find('form').serializeObject();

			if (data.password != data.password2) {
				window.alert('Пароли не совпадают!');
				return;
			}

			function getQueryVariable(variable) {
		       var query = window.location.search.substring(1);
		       var vars = query.split("&");
		       for (var i=0;i<vars.length;i++) {
		               var pair = vars[i].split("=");
		               if(pair[0] == variable){return pair[1];}
		       }
		       return;
			}

			var token = getQueryVariable('token');
            if (token) {
                data.token = token;
            }

			self.ui.alert.hide();
			
			$.ajax({
		        type: 'POST', 
	        	url: '/api/auth/reset',
		        data: data,
		        dataType: 'json' 
		    })
		        .done(function( response ) {
					window.location.href = '/';
		        })
		        .fail(function(jqXHR, textStatus, err) {
		            self.ui.alert.text(JSON.parse(jqXHR.responseText).error);
		            self.ui.alert.show();

					self.ui.password.focus();
		        });
		}
    })
});
