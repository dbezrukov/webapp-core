define([
    'jquery',
    'underscore',
    'marionette',
    'translit',
    'tpl!templates/auth/signup_pane.tmpl'

], function($, _, Marionette, translit, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'padding-top',

        ui: {
            alert: '.alert',
            displayName: 'input[name="displayName"]',
            name: 'input[name="name"]', // can be used in product forum
            phone: 'input[name="phone"]',
            phoneConfirmationCode: 'input[name="phoneConfirmationCode"]',
            consent: 'input[name="consent"]'
        },

        events: {
            'click a.resend': 'resendSMSClicked',
        	'click a.signup': 'signupClicked',
        	'keypress input': 'keyPressed',
            'change input[name="displayName"]': 'displayNameChanged'
        },

        onDomRefresh: function() {
  			
  			var self = this;
        	self.ui.displayName.focus();
		},

		keyPressed: function(event) {
        	var self = this;

        	if (event.keyCode == 13) {
        		self.signupClicked();

        		event.preventDefault();
        		return false;
        	}
        },

        displayNameChanged: function(event) {
            var self = this;

            this.ui.name.val(this.suggestName(this.ui.displayName.val()));
        },

		resendSMSClicked: function() {
            var self = this;

            var data = $(self.el).find('form').serializeObject();

            $.ajax({
                type: 'POST', 
                url: '/api/auth/resend-sms',
                data: data,
                dataType: 'json' 
            })
                .done(function( response ) {
                    self.ui.phoneConfirmationCode.focus();
                })
                .fail(function(jqXHR, textStatus, err) {
                    self.ui.alert.text(JSON.parse(jqXHR.responseText).error);
                    self.ui.alert.show();

                    self.ui.phone.focus();
                });
        },

        signupClicked: function() {
			var self = this;

			var data = $(self.el).find('form').serializeObject();

			if (data.password != data.password2) {
				self.ui.alert.text('Пароли не совпадают.');
                self.ui.alert.show();
				return;
			}

            if (!this.ui.consent.prop('checked')) {
                self.ui.alert.text('Чтобы продолжить, вам необходимо дать согласие на обработку персональных данных.');
                self.ui.alert.show();
                return;
            }

			function getQueryVariable(variable) {
               var query = window.location.search.substring(1);
               var vars = query.split("&");
               for (var i=0;i<vars.length;i++) {
                       var pair = vars[i].split("=");
                       if(pair[0] == variable){return pair[1];}
               }
               return;
            }

            var inviter = getQueryVariable('ref');
            if (inviter) {
                data.inviter = inviter;
            }

            self.ui.alert.hide();
			
			$.ajax({ 
		        type: 'POST', 
	        	url: '/api/auth/signup/local',
		        data: data,
		        dataType: 'json' 
		    })
		        .done(function(response) {
                    console.log(response);
					window.location.href = '/account';
		        })
		        .fail(function(jqXHR, textStatus, err) {
		            self.ui.alert.text(JSON.parse(jqXHR.responseText).error);
		            self.ui.alert.show();

					self.ui.displayName.focus();
		        });
		}, 

        suggestName: function(displayName) {
            var fio = displayName.trim().toLowerCase().split(' ');
            return translit(fio[0])[0] + translit(fio[1]);
        }
    })
});
