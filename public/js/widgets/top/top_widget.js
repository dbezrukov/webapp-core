define([
    'jquery',
    'underscore',
    'marionette',
    'tpl!./top_widget.tmpl',
    'css!./top_widget'

], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'top-widget half-padding-top',

        onRender: function() {
        },
    });
});
