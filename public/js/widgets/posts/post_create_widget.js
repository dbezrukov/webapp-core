define([
    'jquery',
    'underscore',
    'marionette',

    'widgets/flow_gallery/flow_gallery_widget',
    'widgets/flow_gallery/data/images_cloud',

    'views/auth/login_modal',

    'tpl!./post_create_widget.tmpl',
    'css!./post_create_widget',

    'textarea-autosize'

], function($, _, Marionette,
    FlowGalleryWidget,
    PostImages,
    LoginModal,
    template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'post-create-widget widget padded-wrapper',

        ui: {
            flowGalleryWidgetWrapper: '.flow-gallery-widget-wrapper',
            textArea: '.post-text',
            upload: '.upload'
        },

        events: {
            'click a.upload': 'upload',
            'click a.post': 'post',
            'click textarea': 'textClicked',
        },

        templateHelpers: {
            helperPhoto: function() {
                return app.account.get('photostorage') + app.account.get('photo');
            },
            helperAuthorized: function() {
                return app.account.get('_id');
            },
            helperPathname: function() {
                return window.location.pathname;
            }
        },

        onRender: function() {
            var self = this;

            self.ui.textArea.textareaAutoSize();

            self.files = new PostImages();

            self.initGallery();
        },

        upload: function() {
            var self = this;

            //http://cloudinary.com/documentation/upload_widget
            cloudinary.openUploadWidget({
                    cloud_name: app.account.get('cloudinary').cloud_name,
                    api_key: app.account.get('cloudinary').api_key,
                    upload_preset: 'attachments',
                    text: {
                        'powered_by_cloudinary': 'Powered by Cloudinary - Image management in the cloud',
                        'sources.local.title': 'Файлы',
                        'sources.local.drop_file': 'Перетащите файл сюда',
                        'sources.local.drop_files': 'Перетащите файлы сюда',
                        'sources.local.drop_or': 'Или',
                        'sources.local.select_file': 'Выбрать файл',
                        'sources.local.select_files': 'Выбрать файлы',
                        'sources.url.title': 'Web адрес',
                        'sources.url.note': 'Ссылка на файл:',
                        'sources.url.upload': 'Загрузить',
                        'sources.url.error': 'Пожалуйста укажите верную ссылку',
                        'sources.camera.title': 'Камера',
                        'sources.camera.note': 'Убедитесь, что доступ к камере разрешен и нажмите "Сделать снимок":',
                        'sources.camera.capture': 'Сделать снимок',
                        'progress.uploading': 'Загрузка...',
                        'progress.upload_cropped': 'Загрузить',
                        'progress.processing': 'Обработка...',
                        'progress.retry_upload': 'Попробовать еще раз',
                        'progress.use_succeeded': 'OK',
                        'progress.failed_note': 'Часть изображений не была загружена'
                    }
                },
                function(error, result) {

                    if (error) {
                        console.log('file uploading error: ' + error);
                        return;
                    }

                    if (!result) {
                        return;
                    }

                    result.forEach(function(file) {
                        self.files.push({
                            _id: file.public_id,
                            src: file.secure_url,
                            src_thumb: file.thumbnail_url
                        })
                    })
                });
        },

        post: function() {
            var self = this;

            var options = {
                type: 'POST',
                wait: true,
                success: function(model, response) {
                    self.ui.textArea.val('');
                    self.files.reset();
                },
                error: function(model, response) {
                    console.log(response.responseJSON ?
                        response.responseJSON.error :
                        response.responseText);
                }
            }

            var text = self.ui.textArea.val();
            if (!text && !self.files) {
                return;
            }

            var data = {
                text: text || '',
                files: self.files.toJSON(),
                root: self.model.get('root'),
                
            }

            // user will be populated after creation
            self.collection.create(data, options);
        },

        initGallery: function() {
            var self = this;

            self.flowGalleryWidget = new FlowGalleryWidget({
                collection: self.files,
                canSelect: false,
                canDelete: true,
                previewMode: true
            });

            self.ui.flowGalleryWidgetWrapper.append(self.flowGalleryWidget.render().el);
        },

        textClicked: function() {

            if (!app.account.get('_id')) {
                app.layout.modal.show(new LoginModal({
                    model: app.account
                }));
            }
        }
    });
});