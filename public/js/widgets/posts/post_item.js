define([
    'marionette',

    'widgets/flow_gallery/flow_gallery_widget',
    'widgets/flow_gallery/data/images_cloud',

    'markdown-it', // https://markdown-it.github.io

    'tpl!./post_item.tmpl'

], function(Marionette, 

    FlowGalleryWidget,
    PostImages,

    MarkdownIt,

    template) {

    var md = new MarkdownIt();

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'li',
        className: 'post-item widget padded-wrapper',
        
        ui: {
            textArea: '.post-text',
            flowGalleryWidgetWrapper: '.flow-gallery-widget-wrapper',
        },

        events: {
            //'click .post-text': 'textClicked'
        },

        templateHelpers: {
            helperPhoto: function() {
                return app.account.get('photostorage') + this.user.photo;
            },
            helperUpdated: function() {
                var date = new Date(this.updated);
                var updatedLocal = date.toLocaleString();
                return updatedLocal.substr(0, updatedLocal.lastIndexOf(':'));
            },
            helperPinText: function() {
                return this.pinned 
                    ? 'запись закреплена'
                    : '';
            },
            helperHtmlText: function() {
                return md.render(this.text);
            },
            helperEditable: function() {
                return app.account.get('isModerator') || app.account.get('_id') === this.user._id;
            }
        },

        onRender: function() {
            var self = this;

            if (self.model.get('files').length > 0) {
                self.initGallery();
            }
        },

        initGallery: function() {

            var self = this;

            var files = new PostImages(self.model.get('files'), {
                parse: true
            });

            self.flowGalleryWidget = new FlowGalleryWidget({
                collection: files,
                canSelect: false,
                canDelete: false
            });

            self.ui.flowGalleryWidgetWrapper.append(self.flowGalleryWidget.render().el);
        },

        textClicked: function() {
            var self = this;

            app.navigate('/posts/' + self.model.get('_id'), true);
        }
    });
});