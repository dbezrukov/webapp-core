define([
    'marionette',

    'widgets/flow_gallery/flow_gallery_widget',
    'widgets/flow_gallery/data/images_cloud',

    'tpl!./post_preview_item.tmpl',
    'css!./post_preview_item'

], function(Marionette, 

    FlowGalleryWidget,
    PostImages,

    template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'li',
        className: 'col-xs-12 col-sm-6 col-md-4 col-lg-4',
        
        ui: {
        },

        templateHelpers: {
            helperHref: function() {
                return this.href 
                    ? this.href
                    : '/posts/' + this._id;
            },
            helperPhoto: function() {
                var src = '';
                if (this.files && this.files.length > 0) {
                    src = app.account.get('photostorage') + 'c_scale,w_300/' + this.files[0]._id;
                }
                
                return src;
            },
            helperAvatar: function() {
                return app.account.get('photostorage') + this.user.photo;
            },
            helperUpdated: function() {
                var date = new Date(this.updated);
                var updatedLocal = date.toLocaleString();
                return updatedLocal.substr(0, updatedLocal.lastIndexOf(':'));
            },
            helperPinText: function() {
                return this.pinned 
                    ? 'запись закреплена'
                    : '';
            },
            helperHtmlText: function() {
                var text = this.text;

                var exp = /\r\n|\n|\r/g;
                var html = text.replace(exp, '<br/>');

                return html;
            }
        },

        onRender: function() {
            var self = this;

            if (self.model.get('files').length > 0) {
                self.initGallery();
            }
        },

        initGallery: function() {

            var self = this;

            var files = new PostImages(self.model.get('files'), {
                parse: true
            });
        },

        textClicked: function() {
            var self = this;

            app.navigate('/posts/' + self.model.get('_id'), true);
        }
    });
});