define([
    'jquery',
    'underscore',
    'marionette',

    './post_item',

    'widgets/posts/posts_widget',
    'widgets/posts/post_create_widget',
    'widgets/posts/data/posts',

    'tpl!./post_pane.tmpl'

], function($, _, Marionette,
    PostItem,
    PostsWidget,
    PostCreateWidget,
    Posts,
    template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'post-pane',

        ui: {
            postWidgetWrapper: '.flow-gallery-widget',
            postCreateWidgetWrapper: '.post-create-widget-wrapper',
            postsWidgetWrapper: '.posts-widget-wrapper'
        },

        events: {
            'click a.pin': 'pin',
            'click a.edit': 'edit',
            'click a.delete': 'delete'
        },

        modelEvents: {
            'change': 'render'
        },

        templateHelpers: {
            helperTitle: function() {
                var textLimited = this.text.slice(0, 50);
                var lastSpace = textLimited.lastIndexOf(' ');
                if (lastSpace > 0) {
                    textLimited = textLimited.substr(0, );
                }

                return textLimited + '...';
            },
            helperPin: function() {
                return this.pinned ? 'Открепить' : 'Закрепить';
            },
            helperEditable: function() {
                return app.account.get('isModerator');
            },
            helperAuthorized: function() {
                return app.account.get('_id');
            }
        },

        onRender: function() {
            var self = this;

            self.initPost();

            self.posts = new Posts([], {
                root: self.model.get('_id'),
                order: 'asc' // newer post are at the bottom
            });

            self.initLikes();
            self.initPosts();
            self.initPostCreate();
        },

        initPost: function() {
            var self = this;

            var postWidget = new PostItem({ 
                model: self.model
            });

            self.ui.postWidgetWrapper.append(postWidget.render().el);
        },

        initLikes: function() {
            var self = this;

            if (typeof VK === 'undefined') {
                return;
            }
            
            var postId = self.model.get('_id');
            var pageId = 'post_' + postId;
            var pageUrl = document.origin + '/posts/' + postId;

            VK.Widgets.Like('vk_like', {
                type: 'button',
                pageTitle: app.account.get('app') + '. Новости',
                pageUrl: pageUrl
            }, pageId);
        },

        initPostCreate: function() {
            var self = this;

            var postCreateWidget = new PostCreateWidget({ 
                collection: self.posts,
                model: new Backbone.Model({
                    root: self.model.get('_id'),
                    placeholder: 'Ваш комментарий..'
                })
            });

            self.ui.postCreateWidgetWrapper.append(postCreateWidget.render().el);
        },

        initPosts: function() {
            var self = this;

            self.posts.fetch({
                success: function() {
                    
                    var postsWidget = new PostsWidget({ 
                        collection: self.posts 
                    });

                    self.ui.postsWidgetWrapper.append(postsWidget.render().el);
                },
                error: function() {

                    console.log('cannot fetch posts');
                }
            });
        },

        pin: function() {
            var self = this;

            var data = {
                pinned: !self.model.get('pinned')
            };

            var options = {
                type: 'POST',
                wait: true,
                patch: true,
                success: function (model, response) {
                },
                error: function (model, response) {
                    console.log('post update error');
                }
            }
                
            self.model.save(data, options);
        },

        edit: function() {
            var self = this;

            app.navigate('/posts/' + self.model.get('_id') + '/edit', { trigger: true });
        },

        delete: function() {
            var self = this;

            var confirmation = confirm('Удалить этот пост?');

            if (confirmation !== true) {
                return;
            }

            self.model.destroy();

            app.navigate('/news', { trigger: true });
        }
    })
});
