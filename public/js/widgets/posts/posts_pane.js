define([
    'jquery',
    'underscore',
    'marionette',

    './post_preview_item',
    './post_create_widget',
    './data/posts',

    'tpl!./posts_pane.tmpl'

], function($, _, Marionette,
    PostItem,
    PostCreateWidget,
    Posts,
    template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'posts-pane',

        ui: {
            postCreateWidgetWrapper: '.post-create-widget-wrapper',
            postsWidgetWrapper: '.posts-widget-wrapper'
        },

        events: {
        },

        templateHelpers: {
        },

        onRender: function() {
            var self = this;

            self.posts = new Posts([], {
                root: self.model.get('root')
            });

            if (app.account.get('isAdmin')) {
                self.initPostCreate();
            }

            self.initPosts();
        },

        initPostCreate: function() {
            var self = this;

            var postCreateWidget = new PostCreateWidget({ 
                collection: self.posts,
                model: new Backbone.Model({
                    root: self.model.get('root'),
                    placeholder: 'Создать новый пост..'
                })
            });

            self.ui.postCreateWidgetWrapper.append(postCreateWidget.render().el);
        },

        initPosts: function() {
            var self = this;

            self.posts.fetch({
                success: function() {
                    
                    var PostsWidget = Marionette.CollectionView.extend({
                        tagName: 'ul',
                        className: 'flow-gallery-widget',
                    });

                    var postsWidget = new PostsWidget({ 
                        collection: self.posts,
                        itemView: PostItem
                    });

                    self.ui.postsWidgetWrapper.append(postsWidget.render().el);
                },
                error: function() {

                    console.log('cannot fetch posts');
                }
            });
        }
    })
});
