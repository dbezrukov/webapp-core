define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({

        idAttribute: '_id',
        urlRoot: '/api/posts',

        defaults: function() {
            return {
                text: '',
                files: ''
            }
        }
    });
});