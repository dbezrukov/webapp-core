define([
    'backbone',
    './post'

], function(Backbone, ItemModel) {
    
    return Backbone.Collection.extend({
        
        model: ItemModel,

        sort_key: 'updated',

        comparator: function(item) {
            var sign = this.options.order === 'asc' ? 1 : -1;

            if (item.get('pinned')) {
                return sign*item.get('updated') * 1000;
            } else {
                return sign*item.get('updated');
            }
        },

        initialize: function(models, options) {
            this.options = options || {};
        },

        url: function() { 
            var query = {}

            if (this.options.user) {
                query['user'] = this.options.user;
            }

            if (this.options.since) {
                query['since'] = this.options.since;
            }

            if (this.options.root) {
                query['root'] = this.options.root;
            }

            if (this.options.order) {
                query['order'] = this.options.order;
            }

            return '/api/posts?' + $.param(query);
        }
    });
});
