define([
    'jquery',
    'underscore',
    'marionette',
    './post_item',
    'css!./posts_widget',

], function($, _, Marionette, ItemView) {

    return Marionette.CollectionView.extend({
        tagName: 'ul',
        className: 'posts-widget',
        itemView: ItemView,
        
        collectionEvents: {
            'sync': 'render'
        }
    });
});