define([
    'underscore',
    'marionette',

    'widgets/flow_gallery/flow_gallery_widget',
    'widgets/flow_gallery/data/images_cloud',
    
    'tpl!./post_editor.tmpl',
    'jquery.serializeobject'

], function(_, Marionette, FlowGalleryWidget, PostImages, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'post-editor',

        ui: {
            'alert': '.alert',
            'flowGalleryWidgetWrapper': '.flow-gallery-widget-wrapper',
            'upload': '.upload'
        },

        events: {
            'click a.upload': 'upload',
            'click a.save': 'save',
            'click a.cancel': 'cancel',
            'click a.delete': 'delete'
        },

        templateHelpers: {
            editable: function() {
                return app.account.get('_id');
            },
            created: function() {
                return !!this._id;
            },
            helperTitle: function() {
                var textLimited = this.text.slice(0, 50);
                textLimited = textLimited.substr(0, textLimited.indexOf('<br>') || textLimited.lastIndexOf(' '));

                return textLimited + '...';
            }
        },

        onRender: function() {
            var self = this;

            self.files = new PostImages(self.model.get('files'), {
                parse: true
            });

            self.initGallery();
        },

        initGallery: function() {
            var self = this;

            self.flowGalleryWidget = new FlowGalleryWidget({
                collection: self.files,
                canSelect: false,
                canRotate: true,
                canDelete: true,
                canSort: true
            });

            self.ui.flowGalleryWidgetWrapper.append(self.flowGalleryWidget.render().el);
        },

        save: function() {
            var self = this;

            var data = $(this.el).find('form').serializeObject();
            data.files = self.files.toJSON();
            
            this.ui.alert.hide();

            var options = {
                type: 'POST',
                wait: true,
                success: (model, response) => {
                    app.navigate('/posts/' + model.get('_id'), {
                        trigger: true
                    });
                },
                error: (model, response) => {
                    this.ui.alert.text(response.responseJSON ?
                        response.responseJSON.error :
                        response.responseText);

                    this.ui.alert.show();
                    this.ui.alert.focus();
                }
            }

            this.model.save(data, options);
        },

        cancel: function() {
            var url = this.model.isNew()
                ? '/news'
                : '/posts/' + this.model.get('_id');

            app.navigate(url, { trigger: true });
        },

        delete: function() {
            var confirmation = confirm('Удалить эту новость?');

            if (confirmation !== true) {
                return;
            }

            this.model.destroy();

            app.navigate('/news', {
                trigger: true
            });
        },

        upload: function() {
            var self = this;

            //http://cloudinary.com/documentation/upload_widget
            cloudinary.openUploadWidget({
                    cloud_name: app.account.get('cloudinary').cloud_name,
                    api_key: app.account.get('cloudinary').api_key,
                    upload_preset: 'attachments',
                    text: {
                        'powered_by_cloudinary': 'Powered by Cloudinary - Image management in the cloud',
                        'sources.local.title': 'Файлы',
                        'sources.local.drop_file': 'Перетащите файл сюда',
                        'sources.local.drop_files': 'Перетащите файлы сюда',
                        'sources.local.drop_or': 'Или',
                        'sources.local.select_file': 'Выбрать файл',
                        'sources.local.select_files': 'Выбрать файлы',
                        'sources.url.title': 'Web адрес',
                        'sources.url.note': 'Ссылка на файл:',
                        'sources.url.upload': 'Загрузить',
                        'sources.url.error': 'Пожалуйста укажите верную ссылку',
                        'sources.camera.title': 'Камера',
                        'sources.camera.note': 'Убедитесь, что доступ к камере разрешен и нажмите "Сделать снимок":',
                        'sources.camera.capture': 'Сделать снимок',
                        'progress.uploading': 'Загрузка...',
                        'progress.upload_cropped': 'Загрузить',
                        'progress.processing': 'Обработка...',
                        'progress.retry_upload': 'Попробовать еще раз',
                        'progress.use_succeeded': 'OK',
                        'progress.failed_note': 'Часть изображений не была загружена'
                    }
                },
                function(error, result) {

                    if (error) {
                        console.log('file uploading error: ' + error);
                        return;
                    }

                    if (!result) {
                        return;
                    }

                    result.forEach(function(file, index) {

                        self.files.add([{
                            _id: file.public_id
                        }], {
                            parse: true
                        })
                    })
                });
        }
    });
});