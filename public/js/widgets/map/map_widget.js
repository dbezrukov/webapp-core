define([
    'underscore',
    'marionette',
    'ndvi',
    'tpl!./map_widget.tmpl',
    'js-cookie',
    'leaflet-groupedlayercontrol',
    'css!bower_components/bower_components/leaflet-groupedlayercontrol/dist/leaflet.groupedlayercontrol.min.css'

], function(_, Marionette, Ndvi, template, Cookies) {

    var startLevel = 12;
    var centerLon = 37.620568;
    var centerLat = 55.754056;

    L.Map = L.Map.extend({
        openPopup: function(popup) {
            this._popup = popup;

            return this.addLayer(popup).fire('popupopen', {
                popup: this._popup
            });
        }
    });

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'widget map-widget',

        ui: {
            'map': '.map',
        },

        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;

            var boundsStr = Cookies.get('map-bounds');
            if (boundsStr) {
                var bounds = JSON.parse(boundsStr);
                var corner1 = L.latLng(bounds._northEast.lat, bounds._northEast.lng);
                var corner2 = L.latLng(bounds._southWest.lat, bounds._southWest.lng);
                self.initialBounds = L.latLngBounds(corner1, corner2);
            }
        },

        onRender: function() {

            var self = this;

            self.initMap();
        },

        initMap: function() {
            var self = this;

            var redraw = !!self.map;

            self.layerGoogle = new L.TileLayer('https://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}&s=Ga');
            self.layerOSM = new L.TileLayer(self.options.urlTiles ?
                self.options.urlTiles :
                'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

            self.map = L.map(self.ui.map[0], {
                center: new L.LatLng(centerLat, centerLon),
                zoom: startLevel,
                minZoom: 0,
                maxZoom: 18,
                crs: L.CRS.EPSG3857,
                layers: [self.layerOSM],
                zoomControl: false
            });

            self.map.on('moveend', function(e) {
                var bounds = self.map.getBounds();
                Cookies.set('map-bounds', JSON.stringify(bounds), { expires: 365 });
            });

            /*
            addGeoJsonLayer();

            function addGeoJsonLayer() {
                self.geojsonLayer = new L.GeoJSON(null, {
                    style: function(feature) {
                        return {
                            fillColor: Ndvi.ndviColor(feature.properties['NVDI']),
                            color: '#333',
                            weight: 1,
                            opacity: 1,
                            fillOpacity: 1
                        }
                    },
                    onEachFeature: function(feature, layer) {
                        if (feature.properties) {

                            var popupString = '<div class="popup">';
                            for (var k in feature.properties) {
                                var v = feature.properties[k];
                                popupString += k + ': ' + v + '<br />';
                            }
                            popupString += '</div>';
                            layer.bindPopup(popupString, {
                                maxHeight: 200
                            });
                        }
                    }
                });

                self.map.addLayer(self.geojsonLayer);
            }
            */
        },

        onShow: function() {
            var self = this;

            self.map.invalidateSize(false);

            // restore initial bounds
            if (self.initialBounds) {
                self.map.fitBounds(self.initialBounds);
            }

            /*
            if (self.geojsonLayer.toGeoJSON().features.length > 0) {
                self.map.fitBounds(self.geojsonLayer.getBounds());
            }
            */

            if (!self.map.searchControl) {
                addSearchControl();
            }

            if (!self.map.layersControl) {
                addLayersControl();
            }

            function addSearchControl() {
                var GoogleSearch = L.Control.extend({
                    options: {
                        position: 'topright',
                    },
                    onAdd: function() {
                        var element = document.createElement("input");
                        element.id = "searchBox";
                        return element;
                    }
                });

                (new GoogleSearch).addTo(self.map);

                var input = document.getElementById("searchBox");

                self.map.searchControl = new google.maps.places.SearchBox(input);

                self.map.searchControl.addListener('places_changed', function() {
                    var places = self.map.searchControl.getPlaces();

                    if (places.length == 0) {
                        return;
                    }

                    var group = L.featureGroup();

                    places.forEach(function(place) {

                        var marker = L.marker([
                            place.geometry.location.lat(),
                            place.geometry.location.lng()
                        ]);

                        group.addLayer(marker);
                    });

                    group.addTo(self.map);
                    self.map.fitBounds(group.getBounds());
                })
            }

            function addLayersControl() {
                var baseLayers = {
                    'Google': self.layerGoogle,
                    'OSM': self.layerOSM
                };

                var agricultureOpacity = 0.6;
                
                /*
                self.overlayNdvi = self.options.ndviBoundary && self.options.ndviBoundary.coordinates.length > 0
                ? new L.TileLayer.BoundaryCanvas(
                    'https://tms.okocenter.ru/data/ndvi/gmt/2016-06-05/{z}/{x}/{y}.png', {
                    opacity: agricultureOpacity,
                    boundary: self.options.ndviBoundary
                })
                : new L.TileLayer(
                    'https://tms.okocenter.ru/data/ndvi/gmt/2016-06-05/{z}/{x}/{y}.png', {
                    opacity: agricultureOpacity
                });
                */

                var overlayVisual = new L.TileLayer(
                    'https://tms.okocenter.ru/data/visual/gmt/2016-06-05/{z}/{x}/{y}.png', {
                        opacity: agricultureOpacity
                    }
                );

                /*
                var overlayCadastre = L.tileLayer.Rosreestr(
                    'http://{s}.pkk5.rosreestr.ru/arcgis/rest/services/Cadastre/Cadastre/MapServer/export?dpi=96&transparent=true&format=png32&bbox={bbox}&size=256,256&bboxSR=102100&imageSR=102100&f=image', {
                        tileSize: 256,
                        clickable: true,
                        attribution: 'Росреестр'
                    }
                )
                */

                var groupedOverlays = {
                    'HD снимки': {
                        'Visual': overlayVisual,
                        //'NDVI': self.overlayNdvi
                    },
                    /*
                    'Кадастровая карта': {
                        'Россия': overlayCadastre
                    }*/
                };

                if (self.options.userLayers) {
                    groupedOverlays['Пользовательские слои'] = _.mapObject(self.options.userLayers, function(value, key) {
                        return value.layer;
                    })

                    // showing user layers
                    _.values(self.options.userLayers).forEach(function(userLayer) {
                        if (userLayer.show) {
                            self.map.addLayer(userLayer.layer);
                        }
                    });
                }

                if (self.options.weatherLayers) {
                    groupedOverlays['Погода'] = self.options.weatherLayers;
                }

                self.map.layersControl = L.control.groupedLayers(baseLayers, groupedOverlays);
                self.map.layersControl.addTo(self.map);
            }
        },

        /*
        setLayerVisible(name, visible) {
            var self = this;

            var userLayer = self.options.userLayers[name];
            if (!userLayer) {
                console.log('layer not found: ' + name);
                return;
            }

            if (visible) {
                self.map.addLayer(userLayer.layer);
            } else {
                self.map.removeLayer(userLayer.layer);
            }
        }
        */
    });
});