/* FlowGallery
    Это галлерея картинок с возможностью их:
    - выбора
    - поворота
    - вращения
    - удаления
    - запуска сканирования текста

    у каждого элемента может быть картинка, ссылка и подпись
*/

define([
    'jquery',
    'underscore',
    'marionette',
    'sortable',
    './flow_gallery_item',
    'lightgallery',
    'lg-fullscreen',
    'lg-video',
    'css!./flow_gallery_widget',
    'css!bower_components/lightgallery/dist/css/lightgallery.min.css',

], function($, _, Marionette, Sortable, ItemView) {

    return Marionette.CollectionView.extend({
        tagName: 'ul',
        className: 'flow-gallery-widget',
        itemView: ItemView,

        collectionEvents: {
            'sync': 'render',
            'change': 'render'
        },

        initialize: function(options) {
            var self = this;

            self.options = options || {};
        },

        onRender: function() {

            var self = this;

            if (self.options.canSelect) {
                $(self.el).attr('can-select', true);
            }

            if (self.options.canRotate) {
                $(self.el).attr('can-rotate', true);
            }

            if (self.options.canScan) {
                $(self.el).attr('can-scan', true);
            }

            if (self.options.canDelete) {
                $(self.el).attr('can-delete', true);
            }

            if (self.options.fullHeight) {
                $(self.el).attr('fullHeight', true);
            }

            
            if (self.options.canSort) {
                
                Sortable.create($(self.el)[0], {
                    handle: 'img',
                    onEnd: self.onEnd.bind(self),
                })
            }

            setTimeout(function() {
                if ($(self.el).data('lightGallery')) {
                    $(self.el).data('lightGallery').destroy(true);
                }

                $(self.el).lightGallery({
                    selector: '.image-wrapper[data-src]'
                });
            });
        },

        onEnd(evt) {
            var self = this;

            function moveItem(from, to) {
                self.collection.models.splice(to, 0, self.collection.models.splice(from, 1)[0]);
            };

            moveItem(evt.oldIndex, evt.newIndex);
        }
    });
});