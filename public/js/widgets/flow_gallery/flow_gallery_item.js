define([
    'marionette',
    'tpl!./flow_gallery_item.tmpl',
    'css!./flow_gallery_item'

], function(Marionette, template) {

	return Marionette.ItemView.extend({
        template: template,
        tagName: 'li',
        className: 'col-xs-12 col-sm-6 col-md-4 col-lg-4',

        ui: {
        	'imageSelect': '.image-select'
        },

        events: {
            'click .image-select': 'selectImage',
            'click .image-rotate': 'rotateImage',
            'click .image-scan':   'scanImage',
            'click .image-delete': 'deleteImage'
        },

        modelEvents: {
        },

        templateHelpers: {
            helperSrc: function() {
                return this.src 
                    ? this.src 
                    : app.account.get('photostorage') + 'c_scale,h_900/' + this._id;
            },
            helperSrcThumb: function() {
                if (this.src_thumb) {
                    return this.src_thumb;
                }
                return this.src
                    ? this.src
                    : app.account.get('photostorage') + 'c_scale,w_300/' + this._id;
            }
        },

		initialize: function(){
            var self = this;
        },

		onRender: function() {
        	var self = this;
            //$(self.ui.imageSelect).attr('selected', !!self.model.get('selected'));
        },

        selectImage: function(event) {
            var self = this;

            var newSelection = !this.model.get('selected');
        	self.setSelected(newSelection);
        },

        rotateImage: function(event) {
            var self = this;

            self.model.rotate({
                context: self,
                success: function() {
                }
            });
        },

        scanImage: function(event) {
            app.vent.trigger('image:scan', this.model);
        },

        deleteImage: function(event) {
            // explicitly delete using Files API
            this.model.destroy();

            // silent deletion without sending DELETE
            //self.model.trigger('destroy', self.model);
        },

        setSelected: function(selected) {
			var self = this;

			self.model.set('selected', selected);
            //$(self.ui.imageSelect).attr('selected', selected);
		}
    });
});
