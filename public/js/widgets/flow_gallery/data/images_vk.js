define([
    'backbone',
    './image'

], function(Backbone, Model) {

    return Backbone.Collection.extend({
        model: Model,

        fetch: function(options) {

        	options.url = 'https://api.vk.com/method/photos.get?owner_id=-121757520&photo_sizes=1&album_id=' + options.albumId;
        	options.dataType = 'jsonp';
        	
        	return Backbone.Collection.prototype.fetch.call(this, options);
        },

        parse: function(response) {
        	var images = [];

    		if (!response.response) {
    			console.log('missing albums for the VK group');
    			return images;
    		}

    		response.response.forEach(function(item) {

				var image = _.findWhere(item.sizes, { type: 'z' }) || _.findWhere(item.sizes, { type: 'x' });
	    		var image_thumb = _.findWhere(item.sizes, { type: 'x' });
	    		
	    		images.push(new Model({
					src: image.src,
					src_thumb: image_thumb.src
					title: image.text
				}));
			})

    		return images;
  		}
    });
});
