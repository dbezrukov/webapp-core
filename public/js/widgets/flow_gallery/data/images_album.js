define([
    'backbone',
    './image'

], function(Backbone, Model) {

    return Backbone.Collection.extend({
        model: Model,

        fetch: function(options) {

        	options.url = '/api/albums/' + options.albumId;
        	
        	return Backbone.Collection.prototype.fetch.call(this, options);
        },

        parse: function(response) {
        	var images = [];

    		response.images.forEach(function(image) {

	    		images.push(new Model({
					src: image.src,
					src_thumb: image.src,
					title: image.text
				}));
			})

    		return images;
  		}
    });
});
