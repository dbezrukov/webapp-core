define([
    'backbone',
    './image'

], function(Backbone, Model) {

    return Backbone.Collection.extend({
        
        model: Model,

        parse: function(files) {
            var images = [];

            files.forEach(function(file) {

                var src = '';
                var src_thumb = '';

                if (file._id.substr(0, 3) === 'vi/') {
                    var videoId = file._id.slice(3);
                    src = 'https://www.youtube.com/watch?v=' + videoId;
                    src_thumb = 'https://img.youtube.com/vi/' + videoId + '/mqdefault.jpg';
                } else {
                    src = app.account.get('photostorage') + 'c_scale,h_900/' + file._id
                    src_thumb = app.account.get('photostorage') + 'c_scale,w_300/' + file._id
                }

                images.push(new Model({
                    _id: file._id,
                    src: file._id ? '' : src,
                    src_thumb: file._id ? '' : src_thumb,
                    title: file.title || '',
                    text: file.text || ''
                }));
            })

            return images;
        }
    });
});
