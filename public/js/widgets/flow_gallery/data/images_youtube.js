define([
    'backbone',
    './image'

], function(Backbone, Model) {

    return Backbone.Collection.extend({
        model: Model,

        fetch: function(options) {

        	options.url = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyA1d8GnBy58BHrOBNveDEMCaM4eTN9AHgU&channelId=' 
        		+ options.albumId + '&part=snippet,id&order=date&maxResults=50';
        	
        	return Backbone.Collection.prototype.fetch.call(this, options);
        },

        parse: function(response) {
        	var images = [];

    		response.items.forEach(function(item) {

	    		var videoId = item.id.videoId;

	    		if (videoId) {
	    			images.push(new Model({
						src: 'https://www.youtube.com/watch?v=' + videoId,
						src_thumb: 'https://img.youtube.com/vi/' + videoId + '/mqdefault.jpg',
						title: item.snippet.title
					}));
	    		}
			})

    		return images;
  		}
    });
});
