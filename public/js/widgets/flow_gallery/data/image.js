define([
    'underscore',
    'backbone'

], function(_, Backbone) {

    return Backbone.Model.extend({
        
        idAttribute: '_id',
        urlRoot: '/api/files',

        defaults: function() {
            return {
            	selected: false,
            	//src: '',
            	//src_thumb: '',
            	title: '',
            	href: ''
            }
        },

        rotate: function(options) {
            var self = this;

            var url = self.get('_id');

            if (url.indexOf('a_90/') > -1) {
                self.set('_id', url.replace('a_90/', 'a_180/'));
            } else if (url.indexOf('a_180/') > -1) {
                self.set('_id', url.replace('a_180/', 'a_270/'));
            } else if (url.indexOf('a_270/') > -1) {
                self.set('_id', url.replace('a_270/', ''));
            } else {
                self.set('_id', 'a_90/' + self.get('_id'));
            }

            options.success();
        }
    });
});
