define([
    'jquery',
    'underscore',
    'marionette',

    './user_item',
    'tpl!./users_pane.tmpl'
    
], function($, _, Marionette, ItemView, template) {

    return Marionette.CompositeView.extend({

        template: template,
        itemView: ItemView,
        itemViewContainer: '.users',
        tagName: 'div',
        className: 'users-pane'
    });
});
