define([
    'marionette',

    'tpl!./user_item.tmpl',
    'css!./user_item'

], function(Marionette, 

    template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'li',
        className: 'user-item padded-wrapper',
        
        ui: {
        },

        templateHelpers: {
            helperPhoto: function() {
                return app.account.get('photostorage') + this.photo;
            }
        },

        onRender: function() {
            var self = this;
        }
    });
});