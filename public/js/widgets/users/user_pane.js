define([
    'jquery',
    'underscore',
    'marionette',

    'widgets/posts/posts_widget',
    'widgets/posts/post_create_widget',
    'widgets/posts/data/posts',

    'tpl!./user_pane.tmpl',
    'css!./user_pane',

], function($, _, Marionette,
    PostsWidget,
    PostCreateWidget,
    Posts,
    template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'user-pane',

        ui: {
            alertDanger: '.alert-danger',
            alertSuccess: '.alert-success',
            postCreateWidgetWrapper: '.post-create-widget-wrapper',
            postsWidgetWrapper: '.posts-widget-wrapper'
        },

        templateHelpers: {
            helperBack: function() {
                return app.account.get('photostorage') + 'assets/user-back';
            },
            helperPhoto: function() {
                return app.account.get('photostorage') + this.photo;
            }
        },

        events: {
            'click img.upload': 'uploadPhoto'
            
        },

        modelEvents: {
            'change': 'render'
        },

        initialize: function() {
            var self = this;

            self.editable = (self.model.get('_id') === app.account.get('_id'));
        },

        onRender: function() {
            var self = this;

            self.posts = new Posts([], {
                user: self.model.get('_id'),
                root: 'news'
            });
            
            if (self.editable) {
                self.initPostCreate();
            }

            self.initPosts();
        },

        initPostCreate: function() {
            var self = this;

            var postCreateWidget = new PostCreateWidget({ 
                collection: self.posts,
                model: new Backbone.Model({
                    root: 'news',
                    placeholder: 'Что у Вас нового?'
                })
            });

            self.ui.postCreateWidgetWrapper.append(postCreateWidget.render().el);
        },

        initPosts: function() {
            var self = this;

            self.posts.fetch({
                success: function() {
                    
                    var postsWidget = new PostsWidget({ 
                        collection: self.posts 
                    });

                    self.ui.postsWidgetWrapper.append(postsWidget.render().el);
                },
                error: function() {

                    console.log('cannot fetch posts');
                }
            });
        },

        uploadPhoto: function() {
            var self = this;

            if (!self.editable) {
                return;
            }

            //http://cloudinary.com/documentation/upload_widget
            cloudinary.openUploadWidget({
                cloud_name: app.account.get('cloudinary').cloud_name, 
                api_key: app.account.get('cloudinary').api_key,
                upload_preset: 'avatars',
                multiple: false,
                cropping: 'server',
                cropping_aspect_ratio: 1,
                cropping_default_selection_ratio: 1,
                text: {
                    'powered_by_cloudinary': 'Powered by Cloudinary - Image management in the cloud',
                    'sources.local.title': 'Файлы',
                    'sources.local.drop_file': 'Перетащите файл сюда',
                    'sources.local.drop_files': 'Перетащите файлы сюда',
                    'sources.local.drop_or': 'Или',
                    'sources.local.select_file': 'Выбрать файл',
                    'sources.local.select_files': 'Выбрать файлы',
                    'sources.url.title': 'Web адрес',
                    'sources.url.note': 'Ссылка на файл:',
                    'sources.url.upload': 'Загрузить',
                    'sources.url.error': 'Пожалуйста укажите верную ссылку',
                    'sources.camera.title': 'Камера',
                    'sources.camera.note': 'Убедитесь, что доступ к камере разрешен и нажмите "Сделать снимок":',
                    'sources.camera.capture': 'Сделать снимок',
                    'progress.uploading': 'Загрузка...',
                    'progress.upload_cropped': 'Загрузить',
                    'progress.processing': 'Обработка...',
                    'progress.retry_upload': 'Попробовать еще раз',
                    'progress.use_succeeded': 'OK',
                    'progress.failed_note': 'Часть изображений не была загружена'
                }
            },
            function(error, result) {
                
                if (error) {
                    console.log('file uploading error: ' + error);
                    return;
                }

                if (!result) {
                    return;
                }

                self.ui.alertDanger.hide();
                self.ui.alertSuccess.hide();
            
                var options = {
                    type: 'POST',
                    wait: true,
                    patch: true,
                    success: function (model, response) {
                        self.ui.alertSuccess.text('Фото обновлено.');
                        self.ui.alertSuccess.show();
                        setTimeout(function() {
                            self.ui.alertSuccess.hide();
                        }, 2000);
                    },
                    error: function (model, response) {
                        self.ui.alertDanger.text(response.responseJSON 
                            ? response.responseJSON.error
                            : response.responseText);
                        self.ui.alertDanger.show();
                    }
                }
            
                self.model.save({
                    photo: result[0].public_id
                }, options);
            });
        }
    })
});
