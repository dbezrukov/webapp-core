define([
    'jquery',
    'underscore',
    'marionette',
    './user_item',
    'css!./users_widget',

], function($, _, Marionette, ItemView) {

    return Marionette.CollectionView.extend({
        tagName: 'ul',
        className: 'users-widget',
        itemView: ItemView,
        
        collectionEvents: {
            'sync': 'render'
        }
    });
});