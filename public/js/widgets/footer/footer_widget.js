define([
    'jquery',
    'underscore',
    'marionette',
    'tpl!./footer_widget.tmpl',
    'css!./footer_widget',
    'css!bower_components/font-awesome/css/font-awesome.min.css'
    
], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'footer-widget padding-bottom'
    });
});