define([
    'underscore',
    'marionette',
    'tpl!./contacts_pane.tmpl'
    
], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'contacts-pane',

        onRender: function() {
            var self = this;

            if ($(self.el).localize) {
                $(self.el).localize();
            }
        }
    });
});
