define([
    'jquery',
    'underscore',
    'marionette',
    
    'views/target_item',

    'css!./targets_widget',

], function($, _, Marionette, ItemView) {

    return Marionette.CollectionView.extend({
        tagName: 'ul',
        className: 'targets-widget',
        itemView: ItemView,
        
        collectionEvents: {
            //'sync': 'render'
        }
    });
});