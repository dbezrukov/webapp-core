// app.js 

var express = require('express');
var app = express();
var mongoose = require('mongoose');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var passport = require('passport');
let cluster = require('cluster');
var compression = require('compression');  

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var path = require('path');
var async = require("async");
var require_tree = require('require-tree');
var favicon = require('serve-favicon');
var robots = require('express-robots');

var fs = require('fs');
var _ = require('underscore');

var config = require('../config');

var Raven = require('raven');

if (config.raven) {
    Raven.config(config.raven.dsn).install();        
}

function initializeApplication() {

    // overriding https://github.com/Automattic/mongoose/issues/4291
    mongoose.Promise = global.Promise;

    global.__coreModules = path.join(__dirname, '/node_modules');

    require('./config/passport')(passport);

    global.app = app;

    app.use(compression());
    
    // The request handler must be the first middleware on the app
    if (config.raven) {
        app.use(Raven.requestHandler());
    }

    if (config.app.debug) {
        app.use(morgan('dev'));
    }

    app.use(cookieParser());

    // override cookies with token comes from mobile client
    app.use(function (req, res, next) {
        
        function getBearer() {
            let bearer = req.header('Authorization');
            return (bearer && bearer.substr(0, 6).toLowerCase() === 'bearer')
                ? bearer.slice(7)
                : ''
        }

        let token = getBearer();

        if (token.length > 0) {
            console.log('Auth: Token found ' + token);
            req.cookies[config.session.name] = token;
        }

        next();
    });

    // parse X-APP-DeviceId info
    app.use(function (req, res, next) {
        req.deviceId = req.header('X-APP-DeviceId');
        next();
    });

    app.use(bodyParser.json({
        limit: '50mb'
    }));
    app.use(bodyParser.urlencoded({
        limit: '50mb',
        extended: true
    }));
    app.use(favicon(path.join(__dirname, '/../frontend/img/favicon.ico')));
    app.use(robots(path.join(__dirname, '/../frontend/robots.txt')));

    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');

    if (config.db) {
        mongoose.connect(config.db);
        mongoose.connection.on('open', function(err, db) {});

        // required for passport
        app.use(session({
            name: config.session.name,
            secret: config.session.secret,
            cookie: { maxAge: 10 * 365 * 24 * 60 * 60 * 1000 },
            saveUninitialized: false,
            store: new MongoStore({
                url: config.db
            })
        }));
    }

    app.use(passport.initialize());
    app.use(passport.session());

    // using both core- and app public folders
    var oneDay = 86400000;
    var oneHour = 3600000;

    // so that you can override UI
    app.use(express.static(path.join(__dirname, '/../frontend'), {
        maxAge: oneHour
    }));
    app.use(express.static(path.join(__dirname, '/../files'), {
        maxAge: oneDay
    }));
    app.use(express.static(path.join(__dirname, '/public'), {
        maxAge: oneHour
    }));
    app.use(express.static(path.join(__dirname, '/api'), {
        maxAge: oneHour
    }));

    // scanning core API routes
    var coreApiRoutes = path.join(__dirname, '/routes/api');
    fs.readdirSync(coreApiRoutes).forEach(function(file) {
        var name = file.substr(0, file.indexOf('.'));
        require(coreApiRoutes + '/' + name)(app, passport);
    });

    // scanning user routes
    var userRoutes = path.join(__dirname, '/../backend/routes');
    try {
        console.log('call stats');
        var stats = fs.statSync(userRoutes);

        console.log('test stats');
        console.log(stats);

        fs.readdirSync(userRoutes).forEach(function(file) {
            var name = file.substr(0, file.indexOf('.'));
            require(userRoutes + '/' + name)(app, passport);
        });
    } catch (err) {
        if (err.code === 'ENOENT') {
            console.log('user backend doesnt exist');
        } else {
            console.log('Got exception while loading user backend: ' + err);
            console.log(err.stack);
            return;
        }
    }

    // include index route
    var indexRoute = path.join(__dirname, '/routes/index');
    require(indexRoute)(app, passport);

    // setup cloudinary image hosting
    if (config.cloudinary) {
        app.cloudinary = require('cloudinary');
        app.cloudinary.config(config.cloudinary);
    }

    // setup matermost user management
    if (config.mattermost) {
        app.mattermost = require('./modules/mattermost');
        app.mattermost.config(config.mattermost);
    }

    // setup LDAP user management
    if (config.ldap) {
        app.ldap = require('./modules/ldap');
        app.ldap.config(config.ldap);
    }

    // The error handler must be before any other error middleware
    if (config.raven) {
        app.use(Raven.errorHandler());
    }

    // starting app
    console.log('Starting Express 4.0 on port ' + config.server.port);
    app.listen(config.server.port);
}

app.getUserConfig = function(user) {
    return {
        id: user.id,
        name: user.name,
        photo: user.photo,
        photostorage: config.cloudinary.photostorage,
        servertime: Date.now(),
    }
}

process.on('uncaughtException', function(err) {
    console.log(err);
    console.log(err.stack);
});

async.parallel({}, function() {
    
    if (cluster.isMaster) {  
        let cpus = require('os').cpus().length;
        console.log('#CLUSTER: CPU core number: ' + cpus);

        for (let i = 0; i < cpus; i += 1) {
            cluster.fork();
        }    
    } else {
        console.log('#CLUSTER: Inializing app');
        initializeApplication();
    }
});