var _ = require('underscore');
var ldap = require('ldapjs');
var async = require('async');

var config = {};

var client;

function getClient(callback) {
	if (client) {
		return callback(null, client);
	}

	client = ldap.createClient({ url: config.url });
	client.bind(config.dn, config.password, function(err) {
		callback(err, client);
	})
}

module.exports = { 
    
    config: function(configData) {
        config = configData;
    },

    addUser: function(ldapId, email, name, password, callback) {
        
        console.log('#ldap addUser');

        async.waterfall([
            // create ldap client
            function(done) {
                console.log('#creating ldap client and binding');
                getClient(done);
            },
            // searching the latest uidNumber in People
            function(client, done) {
                console.log('#searching for the latest id');

                var opts = {
                    scope: 'sub',
                    attributes: ['uidNumber']
                };
            
                client.search('ou=People,dc=okocenter,dc=ru', opts, function(err, res) {

                    if (err) {
                        return done('LDAP error: ' + JSON.stringify(err));
                    }
                    
                    var maxUidNumber = 0;

                    res.on('searchEntry', function(entry) {
                        var uidNumber = parseInt(entry.object.uidNumber);
                        if (uidNumber > maxUidNumber) {
                            maxUidNumber = uidNumber;
                        }
                    });
                    res.on('error', function(err) {
                        return done(err.message);
                    });
                    res.on('end', function(result) {
                        done(null, client, ++maxUidNumber);
                    });
                });
            },
            /*
            // searching the default gidNumber
            function(client, uidNumber, done) {
                console.log('#searching for the default gidNumber');

                var opts = {
                    attributes: ['gidNumber']
                };
            
                client.search('cn=People,ou=Group,dc=okocenter,dc=ru', opts, function(err, res) {

                    if (err) {
                        return done(err);
                    }
                    
                    var maxUidNumber = 0;

                    res.on('searchEntry', function(entry) {
                        return done(null, client, uidNumber, entry.object.gidNumber);
                    });
                    res.on('error', function(err) {
                        return done(err.message);
                    });
                });
            },
            */
            // adding user entity
            function(client, uidNumber, done) {
                console.log('#adding user entity');
                console.log('#uid: ' + uidNumber);

                var entry = {
                    givenName: name.split(' ')[1] || name, // F[I]O
                    sn: name.split(' ')[0] || name, // [F]IO
                    cn: name,
                    objectclass: ['posixAccount', 'inetOrgPerson'],
                    mail: email,
                    uidNumber: uidNumber,
                    gidNumber: config.gidNumber,
                    homeDirectory: '/home/' + ldapId,
                    userPassword: password
                };

                console.log(entry);

                var dn = 'uid=' + ldapId + ',ou=People,dc=okocenter,dc=ru';
                client.add(dn, entry, function(err) {
                    
                    if (err) {
                        return done('LDAP error: ' + JSON.stringify(err));
                    }

                    done(null);
                });

                /*
                var change = new ldap.Change({
                    operation: 'add',
                    modification: {
                      memberUid: 777
                    }
                });

                client.modify('cn=okocenter,ou=Group,dc=okocenter,dc=ru', change, function(err, res) {
                    if (err) {
                        console.error("Looks like group add FAILED: %j", err);
                    } else {
                        console.log("Looks like group add WORKED: %j", res);
                    }
                });
                */
            }


        ], function(err) {
            callback(err);
        });
    },

    getUser: function(email, callback) {
        
        console.log('#ldap getUser');

        // just for test, can't test ldap now
        return callback(null, 'id_' + email);

        async.waterfall([
            // create ldap client
            function(done) {
                console.log('#creating ldap client and binding');
                getClient(done);
            },
            // searching the latest uidNumber in People
            function(client, done) {
                console.log('#searching user by email');

                var opts = {
                    scope: 'sub',
                    attributes: ['uid', 'cn'],
                    filter: '(mail=' + email + ')',
                };
            
                client.search('ou=People,dc=okocenter,dc=ru', opts, function(err, res) {

                    if (err) {
                        return done(err);
                    }
                    
                    var user;

                    res.on('searchEntry', function(entry) {
                        user = entry.object;
                        console.log('ldap entry found:');
                        console.log(user);
                    });
                    res.on('error', function(err) {
                        return done(err.message);
                    });
                    res.on('end', function(result) {
                        done(null, user);
                    });
                });
            },

        ], function(err) {
            callback(err);
        });
    },

    removeUser: function(ldapId, callback) {
        
        console.log('#ldap removeUser');

        async.waterfall([
            // create ldap client
            function(done) {
                console.log('#creating ldap client and binding');
                getClient(done);
            },
            // removing group entity
            function(client, done) {
                console.log('#removing user entity: ' + ldapId);

                var dn = 'uid=' + ldapId + ',ou=People,dc=okocenter,dc=ru';
                client.del(dn, function(err) {
                    if (err) {
                        return done('LDAP error: ' + JSON.stringify(err));
                    }

                    done(null);
                });
            }
        ], function(err) {
            callback(err);
        });
    },

    addGroup: function(name, callback) {

    	console.log('#ldap addGroup');

    	async.waterfall([
			// create ldap client
		    function(done) {
		    	console.log('#creating ldap client and binding');
		    	getClient(done);
		    },
		    // searching the latest gidNumber in Group
		    function(client, done) {
		    	console.log('#searching for the latest id');

		    	var opts = {
  					scope: 'sub',
  					attributes: ['gidNumber']
				};
			
				client.search('ou=Group,dc=okocenter,dc=ru', opts, function(err, res) {

					if (err) {
                        return done('LDAP error: ' + JSON.stringify(err));
                    }

					var maxGidNumber = 0;

					res.on('searchEntry', function(entry) {
						if (entry.object.gidNumber > maxGidNumber) {
							maxGidNumber = entry.object.gidNumber;
						}
  					});
					res.on('error', function(err) {
						return done(err.message);
					});
					res.on('end', function(result) {
						done(null, client, ++maxGidNumber);
					});
				});
			},
		    // adding group entity
		    function(client, gidNumber, done) {
		    	console.log('#adding group entity with uid: ' + gidNumber);

		    	var entry = {
			  		cn: name,
			  		objectclass: ['posixGroup', 'top'],
			  		description: 'group description',
					gidNumber: gidNumber
				};
			
				var dn = 'cn=' + name + ',ou=Group,dc=okocenter,dc=ru';
				client.add(dn, entry, function(err) {
					console.log('add result: ' + err);
					done(err);
				});

  				/*
  				var change = new ldap.Change({
				    operation: 'add',
				    modification: {
				      memberUid: 777
				    }
  				});

  				client.modify('cn=okocenter,ou=Group,dc=okocenter,dc=ru', change, function(err, res) {
    				if (err) {
      					console.error("Looks like group add FAILED: %j", err);
    				} else {
      					console.log("Looks like group add WORKED: %j", res);
    				}
  				});
				*/
			}


		], function(err) {
			callback(err);
		});
    },

    removeGroup: function(name, callback) {
    	
		console.log('#ldap removeGroup');

    	async.waterfall([
			// create ldap client
		    function(done) {
		    	console.log('#creating ldap client and binding');
				getClient(done);
		    },
		    // removing group entity
		    function(client, done) {
		    	console.log('#removing group entity');

				var dn = 'cn=' + name + ',ou=Group,dc=okocenter,dc=ru';
				client.del(dn, function(err) {
					done(err);
				});
			}
		], function(err) {
			callback(err);
		});
    }
}
