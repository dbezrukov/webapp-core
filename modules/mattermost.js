var _ = require('underscore');
var sprintf = require('sprintf-js').sprintf;
var async = require('async');

var ssh = require('ssh-exec');

var config = {};

function exec(cmd, callback) {

    console.log('Executing remote ssh:');
    console.log('ip: ' + config.ip);
    console.log('ssh_user: ' + config.ssh_user);
    console.log('ssh_password: ' + config.ssh_password);

    console.log(cmd);

    ssh(cmd, {
  		host: config.ip,
        user: config.ssh_user,
  		password: config.ssh_password
  	}, 
  	function(err, stdout, stderr) {

  		if (err) {
            var error = 'Mattermost CLI error: ' + err;
            console.log('error ------------------------------');
            console.log(error);
            console.log('stdout -----------------------------');
            console.log(stdout);
            console.log('stderr -----------------------------');
            console.log(stderr);

            if (config.ignoreErrors) {
                console.log('ignoring the error due to config.ignoreErrors option');
                callback();
                return;
            }

  			callback(error);
  		}

  		console.log('ssh command successfully executed');
  		callback();
	})
}

module.exports = { 
    
    config: function(configData) {
    	config = configData;
    },

    /* Method names taken from the following docs:
    	https://docs.mattermost.com/administration/command-line-tools.html */

    userCreate: function(userData, callback) {
    	
    	//sudo ./platform user create --email user@example.com --username userexample --password Password1
		//sudo ./platform user create --firstname Joe --system_admin --email joe@example.com --username joe --password Password1
		
		async.waterfall([
		    
		    function(done) {
		    	console.log('#sending userCreate command for ' + userData.email);

				var cmdTemplate = 'cd /opt/mattermost/bin && ./platform user create --firstname %s --email %s --lastname "%s" --locale %s --nickname %s --username %s --password %s';

				var cmd = sprintf(cmdTemplate,
					userData.firstname,
					userData.email,
					userData.lastname,
					userData.locale,
					userData.nickname,
					userData.username,
					userData.password
				);

				exec(cmd, function(err) {
					done(err);
				});
			}

		], callback);
    },

    userDelete: function(email, callback) {
    	
    	//sudo ./platform user delete user@example.com

		console.log('#sending userDelete command for ' + email);

		var cmdTemplate = 'cd /opt/mattermost/bin && ./platform user delete %s --confirm';
		var cmd = sprintf(cmdTemplate, email);

		exec(cmd, function(err) {
			callback(err);
		})
    }, 

    userPassword: function(email, password, callback) {
    	
		console.log('#setting new password for ' + email);

		var cmdTemplate = 'cd /opt/mattermost/bin && ./platform user password %s %s';
		var cmd = sprintf(cmdTemplate, email, password);

		exec(cmd, function(err) {
			callback(err);
		})
    }, 

    teamCreate: function(name, displayName, callback) {

    	//sudo ./platform team create --name mynewteam --display_name "My New Team"
		//sudo ./platform teams create --name private --display_name "My New Private Team" --private

    	console.log('#sending teamCreate command for ' + name);

		var cmdTemplate = 'cd /opt/mattermost/bin && ./platform team create --name %s --display_name "%s" --private';
		var cmd = sprintf(cmdTemplate, name, displayName);

		exec(cmd, function(err) {
			callback(err);
		})
    },

    teamDelete: function(team, callback) {

    	//sudo ./platform team delete myteam

		console.log('#sending teamDelete command for ' + team);

		var cmdTemplate = 'cd /opt/mattermost/bin && ./platform team delete %s --confirm';
		var cmd = sprintf(cmdTemplate, team);

		exec(cmd, function(err) {
			callback(err);
		})
    },

    teamAdd: function(team, email, callback) {
    	
    	//sudo ./platform team add myteam user@example.com username

		console.log('#sending teamAdd command for ' + email);

		var cmdTemplate = 'cd /opt/mattermost/bin && ./platform team add %s %s && systemctl restart mattermost';
		var cmd = sprintf(cmdTemplate, team, email);


		exec(cmd, function(err) {
			callback(err);
		})
    },

    teamRemove: function(team, email, callback) {

    	//sudo ./platform team remove myteam user@example.com username

		console.log('#sending teamRemove command for ' + email);

		var cmdTemplate = 'cd /opt/mattermost/bin && ./platform team remove %s %s';
		var cmd = sprintf(cmdTemplate, team, email);

		exec(cmd, function(err) {
			callback(err);
		})
    }
}
