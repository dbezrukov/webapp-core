var _ = require('underscore');
var sprintf = require('sprintf-js').sprintf;
var shortid  = require('shortid');
var async = require('async');

var config = require('../../config');

var User = require('../models/user');
var Group = require('../models/group');

function getClient(callback) {
    console.log('init mail client with settings: ');
    console.log(config.mailer);

    if (client) {
        return callback(null, client);
    }

    if (!config.mailer || !config.mailer.user) {
        return callback('mail client is not configured');
    }

    var email = require('emailjs');
    var client = email.server.connect(config.mailer);

    return callback(null, client);
}

function getModerators(callback) {
    
    async.waterfall([
        // find admin group
        function(done) {
            console.log('#find admin group');

            Group
                .findOne({
                    name: 'moderators'
                })
                .exec(done);
        },
        // searching
        function(moderatorsGroup, done) {
            console.log('#searching users');

            User
                .find({
                    groups: { $in: [moderatorsGroup] }
                })
                .exec(done);
        }

    ], function(err, users) {
        if (err) {
            console.log('#error: ' + err);
            return [];
        }

        callback(users);
    });
}

module.exports = {

    sendInvite: function(email, inviteId, inviteeName, inviterName, callback) {
        
        console.log('emailSender: sendInvite');

        async.waterfall([
            // create mailing client
            function(done) {
                console.log('#creating client');
                getClient(done);
            },
            // sending email
            function(client, done) {
                console.log('#actually do the work');

                var confirmationCode = shortid.generate();

                var htmlTemplate = '%s, <br><br> \
                                    %s приглашает вас зарегистрироваться в сервисе ' + config.app.name + '.<br><br> \
                                    <a href="%s" style="%s">Зарегистрироваться</a><br> \
                                    <br>%s';

                var href = config.app.url + '/auth/invite/' + inviteId + '?code=' + confirmationCode;
                var style = 'background-color:#5cb85c;border-radius:3px;color:#ffffff;display:inline-block;font-family:Helvetica,Arial;font-size:14px;line-height:42px;text-align:center;text-decoration:none;width:180px;-webkit-text-size-adjust:none;';

                var nameSurname = inviteeName.split(' ').slice(1);
                inviteeName = nameSurname[0] + ' ' + nameSurname[1];

                var html = sprintf(htmlTemplate, inviteeName, inviterName, href, style, config.mailer.sign);

                console.log('href: ' + href);

                var message = {
                    text: '',
                    from: config.mailer.from,
                    to: email,
                    subject: 'Приглашение в ' + config.app.name,
                    attachment: [{
                        data: html,
                        alternative: true
                    }]
                };

                client.send(message, function(err,  message) { 
                    console.log(err || message);

                    done(err, confirmationCode);
                });
            },

        ], callback);
    },

    sendInviteAccepted: function(email, inviteeName, inviteeDisplayName, inviteeEmail, inviteePhone, callback) {
        
        console.log('emailSender: sendInviteAccepted');

        async.waterfall([
            // create mailing client
            function(done) {
                console.log('#creating client');
                getClient(done);
            },
            // sending email
            function(client, done) {
                console.log('#actually do the work');

                var htmlTemplate = 'Здравствуйте, <br><br> \
                                    по вашему приглашению зарегистрировался пользователь:<br><br> \
                                    Имя: %s<br> \
                                    Полное имя: %s<br> \
                                    Почта: %s<br> \
                                    Телефон: %s<br> \
                                    <br>%s';

                var html = sprintf(htmlTemplate, 
                    inviteeName, inviteeDisplayName, inviteeEmail, inviteePhone, 
                    config.mailer.sign);

                var message = {
                    text: '',
                    from: config.mailer.from,
                    to: email,
                    subject: 'Регистрация по вашему приглашению',
                    attachment: [{
                        data: html,
                        alternative: true
                    }]
                };

                client.send(message, function(err,  message) { 
                    console.log(err || message);

                    done(err);
                });
            },

        ], callback);
    },

    sendRegSuccess: function(email, inviteeName, inviteeDisplayName, callback) {

        console.log('emailSender: sendRegSuccess');

        async.waterfall([
            // create mailing client
            function(done) {
                console.log('#creating client');
                getClient(done);
            },
            // sending email
            function(client, done) {
                console.log('#actually do the work');

                var htmlTemplate = 'Здравствуйте, <br><br> \
                                    Вы успешно зарегистрированы.<br><br> \
                                    Имя для входа на сайт: %s<br> \
                                    Отображаемое имя: %s<br> \
                                    Почта для нотификаций и сброса пароля : %s<br> \
                                    <br>%s';

                var html = sprintf(htmlTemplate, 
                    inviteeName, inviteeDisplayName, email, config.mailer.sign);

                var message = {
                    text: '',
                    from: config.mailer.from,
                    to: email,
                    subject: 'Успешная регистрация',
                    attachment: [{
                        data: html,
                        alternative: true
                    }]
                };

                client.send(message, function(err,  message) { 
                    console.log(err || message);

                    done(err);
                });
            },

        ], callback);

    },

	/*
    sendConfirmation: function(email, requestId, callback) {
    	
		async.waterfall([
			// create mailing client
		    function(done) {
		    	console.log('#creating client');
		    	getClient(done);
		    },
		    // sending email
		    function(client, done) {
		    	console.log('#actually do the work');

		    	var confirmationCode = shortid.generate();

				var htmlTemplate = 'Здравствуйте,<br><br> \
									Вы регистрируетесь на сайте okocenter.ru.<br><br> \
									Для завершения регистрации, пожалуйста, подтвердите ваш электронный адрес. <br><br> \
									<a href="%s" style="background-color:#5cb85c;border-radius:3px;color:#ffffff;display:inline-block;font-family:Helvetica,Arial;font-size:14px;line-height:42px;text-align:center;text-decoration:none;width:180px;-webkit-text-size-adjust:none;">Подтвердить адрес</a>';

				var href = 'https://okocenter.ru/api/requests/confirm-email/' + requestId + '?code=' + confirmationCode;
				var html = sprintf(htmlTemplate, href, href);

				console.log('href: ' + href);

				var message	= {
   					text: '',
					from: 'OkoCenter <info@okocenter.ru>',
   					to: email,
   					subject: 'OkoCenter. Регистрация',
   					attachment: [{
						data: html,
						alternative: true
					}]
				};

				client.send(message, function(err,  message) { 
					console.log(err || message);

					done(err, confirmationCode);
				});
			},

		], callback);
    },

    sendRequestAccepted: function(username, email, token, callback) {
    	
		console.log('sending request confirmed reset link to ' + email);

		async.waterfall([
			// create mailing client
		    function(done) {
		    	console.log('#creating client');
		    	getClient(done);
		    },
		    // sending email
		    function(client, done) {
		    	console.log('#actually do the work');

		    	var htmlTemplate = 'Здравствуйте,<br><br> \
							Заявка на регистрацию подтверждена. <br><br> \
							Для вас создан рабочий почтовый адрес %s. <br><br> \
							Используйте его для входа в ОкоЧат и другие сервисы компании. <br><br> \
							<a href="%s" style="background-color:#5cb85c;border-radius:3px;color:#ffffff;display:inline-block;font-family:Helvetica,Arial;font-size:14px;line-height:42px;text-align:center;text-decoration:none;width:180px;-webkit-text-size-adjust:none;">Задать пароль</a>';

				var href = 'https://okocenter.ru/auth?token=' + token + '#reset';
				var html = sprintf(htmlTemplate, username, href);

		        var message = {
		        	text: '',
		        	from: 'OkoCenter <info@okocenter.ru>',
					to: email,
					subject: 'OkoCenter. Заявка подтверждена',
					attachment: [{
						data: html,
						alternative: true
					}]
				};

				client.send(message, function(err,  message) { 
					console.log(err || message);

					done(err);
				});
			},

		], callback);
    },
    */

    sendPasswordReset: function(email, token, callback) {
    	
		console.log('sending password reset link to ' + email);

		async.waterfall([
			// create mailing client
		    function(done) {
		    	console.log('#creating client');
		    	getClient(done);
		    },
		    // sending email
		    function(client, done) {
		    	console.log('#actually do the work');

		    	var htmlTemplate = 'Здравствуйте,<br><br> \
							Для вашей учетной записи %s запрошен сброс пароля. <br><br> \
							<a href="%s" style="%s">Сбросить пароль</a><br> \
                            <br>%s';

				var href = config.app.url + '/auth/reset?token=' + token;
                var style = 'background-color:#5cb85c;border-radius:3px;color:#ffffff;display:inline-block;font-family:Helvetica,Arial;font-size:14px;line-height:42px;text-align:center;text-decoration:none;width:180px;-webkit-text-size-adjust:none;'

				var html = sprintf(htmlTemplate, email, href, style, config.mailer.sign);

				var message = {
		        	text: '',
		        	from: config.mailer.from,
					to: email,
					subject: 'Сброс пароля',
					attachment: [{
						data: html,
						alternative: true
					}]
				};

				client.send(message, function(err,  message) { 
					console.log(err || message);

					done(err);
				});
			},

		], callback);
    }, 

    notifyModerators: function(subject, htmlText, callback) {

        console.log('notifying moderators');

        async.waterfall([
            // create mailing client
            function(done) {
                console.log('#creating client');
                getClient(done);
            },
            // sending email
            function(client, done) {
                
                console.log('#actually do the work');

                getModerators(function(users) {
                    
                    //users.forEach(function(user))

                    var user = users[0];
                    
                    var message = {
                        text: '',
                        from: config.mailer.from,
                        to: user.email,
                        subject: subject,
                        attachment: [{
                            data: htmlText,
                            alternative: true
                        }]
                    };

                    client.send(message, function(err,  message) { 
                        
                        console.log(err || message);
                        done(err);
                    });
                })
            },

        ], callback);
    }
}
