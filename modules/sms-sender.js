var _ = require(__coreModules + '/underscore');
var sprintf = require(__coreModules + '/sprintf-js').sprintf;
var https = require('https');

module.exports = { 
    
    sendConfirmation: function(phone, callback) {
    	
		var confirmationCode = getRandomInt(1000, 9999).toString();

        console.log(`Sending confirmation code ${confirmationCode} to phone number ${phone}`);

		function getRandomInt(min, max) {
  			return Math.floor(Math.random() * (max - min + 1)) + min;
		}

		var pathTemplate = '/send/?user=%s&password=%s&to=%s&text=%s&from=%s&type=3&answer=json';
		var path = sprintf(pathTemplate, 
			'dmitry.v.bezrukov@gmail.com', 
			'fbeaf71d29bba705d05ada7510295095',
			phone.replace('+', ''),
			'Код подтверждения: ' + confirmationCode,
			'DroneRadar');

		var options = {
			hostname: 'gate.smsaero.ru',
			path: encodeURI(path),
			port: 443
		};

		var req = https.get(options, function(res) {
			var data = ''
				
			res.on('data', function (chunk) {
				data += chunk;
			});
		        
        	res.on('end', function () {

        		var response = JSON.parse(data);
                console.log(response);

                if (response.result === 'reject') {
                    return callback(response.reason);
                }

				callback(null, confirmationCode);
			});
        });

        req.on('error', function (error) {
        	callback(error);
        });

        req.end();
    }
}
