var _ = require(__coreModules + '/underscore');
var sprintf = require(__coreModules + '/sprintf-js').sprintf;

var Post = require('../models/post');

module.exports = { 
    
    makePost: function(text, href, files, callback) {

        // hardcoded Naturometer admin
        var adminId = '599d932397df1421400b0ee4';

        var data = {
            user: adminId,
            text: text,
            href: href,
            files: files,
            root: 'news'
        }
        
        var post = new Post(data);

        post.save(function(err) {
            
            callback(err, post);
        });
    }
}
