WebAppCore
=========
#### A node.js web app core

WebAppCore provides out-of-the-box functionality for running a site that is easily adaptable using config file and a set of standard UI components.

## Table of Contents
1. [Installation](https://bitbucket.org/dbezrukov/webapp-core/overview#markdown-header-installation)
2. [How to use](https://bitbucket.org/dbezrukov/webapp-core/overview#markdown-header-how-to-use)
	
## Installation
------------
Initial Server Setup with CentOS 7.

Installing the latest stable Node.js

``` bash
$ yum install wget
$ cd /usr/src
$ wget https://nodejs.org/dist/v8.2.1/node-v8.2.1.tar.gz
$ tar zxf node-v8.2.1.tar.gz
$ cd node-v8.2.1
$ yum groupinstall 'Development Tools'
$ ./configure
$ make
$ make install
```

Installing MongoDB

Update performance settings prior to Mongo installation. 
https://docs.mongodb.com/manual/reference/ulimit
https://docs.mongodb.com/manual/tutorial/transparent-huge-pages

``` bash
$ sudo rpm --import https://www.mongodb.org/static/pgp/server-3.4.asc

Create /etc/yum.repos.d/mongodb-org-3.4.repo file and add the following section:
[mongodb-org-3.4]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.4/x86_64/
gpgcheck=1
enabled=1

$ yum install mongodb-org
$ systemctl enable mongod
$ systemctl start mongod
```

Installing and optimizing Nginx

``` bash

$ yum install epel-release
$ yum install nginx

Open /etc/nginx/nginx.conf file and add the following to http section:
server_names_hash_bucket_size  64; 

Create /etc/nginx/conf.d/gzip.conf and add the following text:
gzip on;
gzip_proxied any;
gzip_types text/plain text/xml text/css application/x-javascript;
gzip_vary on;
gzip_disable "MSIE [1-6]\.(?!.*SV1)";

$ systemctl enable nginx
$ systemctl start nginx

```

## Cloning project
------------

This project is private, so git access should be configured properly.

``` bash
$ yum update
$ yum install git
copy [id_rsa] key
$ ssh-agent /bin/bash
$ sudo ssh-add [path to id_rsa key]
$ cd /var/www/[project name]/master
$ git clone git@bitbucket.org:dbezrukov/webapp-core.git
$ cd webapp-core
$ npm install
$ cd public
$ npm install -g bower
$ bower install --allow-root
```

## Configuring project
------------

In order to customize your project create a config project file as shown bellow.

``` bash
$ cd /var/www/[project name]

Create config.js and add the following:

module.exports = {
    app: {
    	name: 'Web-service name'
    },

    session : {
        name: '[project name].sid',
        secret: '[project name]',
    },

    server : {
        port : process.env.PORT || 3020
    },
    
    db : 'mongodb://localhost/[project name]', // using default mongodb port 27019
}
```

## Creating frontend
------------

``` bash
$ cd /var/www/[project name]
$ cd /var/www/[project name]/master
$ git clone git@bitbucket.org:dbezrukov/[project name]-frontend.git
$ cd [project name]
$ npm install -g bower
$ bower install --allow-root
```

## Creating backend
------------

``` bash
$ cd /var/www/[project name]
$ cd /var/www/[project name]/master
$ git clone git@bitbucket.org:dbezrukov/[project name]-backend.git
$ cd [project name]
$ npm install
```

## Starting project as a service
------------

``` bash
$ cd /var/www/[project name]
$ cd /var/www/[project name]/master/webapp-core
$ node webapp-core.js
```