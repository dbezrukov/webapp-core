var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var position = mongoose.Schema({
    id: String,
    lat: Number,
    lon: Number,
    utc: Number,
    hgt: Number,
    altbaro: Number,
    pch: Number,
    roll: Number,
    yaw: Number,
    hdg: Number,
    speed: Number,
    source: {
        type: String,
        default: 'gps'
    },
    data: {
        type: Schema.Types.Mixed,
        default: {}
    },
    mode: String
});

module.exports = mongoose.model('Position', position);
