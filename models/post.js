// load the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var uavSchema = mongoose.Schema({

    user: {
        type: Schema.ObjectId, 
        ref: 'User'
    },

    // root: news, post, somewhat item
    root: { 
        type: String
    },

    updated: {
        type : Number, 
        default: Date.now
    },

    text: {
        type: String,
        default: ''
    },

    files: [{
        _id: String // Cloudinary public Id
    }],

    pinned: {
        type : Boolean, 
        default: false
    },

    href: {
        type : String
    }
});

// virtual comment count

// create the model for users and expose it to our app
module.exports = mongoose.model('Post', uavSchema);
