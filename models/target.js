// load the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/* Хранить здесь информацию о БЛА: какие были, когда */

// define the schema for our object model
var uavSchema = mongoose.Schema({
    id: {
		type: String,
		default: ''
    }, 
    name: {
        type: String,
        default: ''
    }, 
	type: {
		type: String,
		default: ''
    },
    subtype: {
        type: String,
        default: ''
    },
    model: {
        type: String,
        default: ''
    },
	updated: {
		type : Number, 
		default: 0 
	},
    valid: {
		type : Boolean, 
		default: false
	},
    position: { type: Schema.ObjectId, ref: 'Position' }, // the most recent valid position
});

// create the model for users and expose it to our app
module.exports = mongoose.model('Target', uavSchema);
