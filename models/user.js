var _ = require('underscore');
var mongoose = require('mongoose');
var mongooseFill = require('mongoose-fill');
var bcrypt   = require('bcrypt-nodejs');
var ldap = require('../modules/ldap');

var Schema = mongoose.Schema;

// Список инвайтов может быть заменен на соотвествующие коллекции
var userSchema = mongoose.Schema({

	name: String,
    email: { type: String, default: '' },
    phone: String,
    displayName: { type: String, default: '' },
    department: { type: String, default: '' },
    position: { type: String, default: '' },
	password: String,
	resetPasswordToken: String,
  	resetPasswordExpires: Date,
	photo : { type: String, default: 'assets/avatar' },

    // groups
    groups: [{ type: Schema.ObjectId, ref: 'Group' }],
    groupsRequested: [{ type: Schema.ObjectId, ref: 'Group' }],
    groupsAdmin: [{ type: Schema.ObjectId, ref: 'Group' }],

    // friendship
    friends: [{ type: Schema.ObjectId, ref: 'User' }],
    requests: [{ type: Schema.ObjectId, ref: 'User' }],
    
    // inviter
    inviter: { type: Schema.ObjectId, ref: 'User' },

    // invitees
    invitees: [{ type: Schema.ObjectId, ref: 'User' }],

    // devices
    devices: [String],

    // other fields
    addInfo: {
		type: Schema.Types.Mixed,
		default: {}
    },

    // social network auth
    social: { type: String },
    socialId: { type: String },

    created: { type : Number, default: Date.now }
}, {
	toObject: { 
	 	virtuals: true,
	 	// toJSON produces undesirable id, so just delete it 
    	transform: function(doc, ret) {
			delete ret.id;
       	}
	},
    toJSON: { 
    	virtuals: true ,
    	// toJSON produces undesirable id, so just delete it 
    	transform: function(doc, ret) {
			delete ret.id;
       	}
    }
});

// optionally ask for the corresponding ldap id
userSchema.fill('ldapId', function (callback) {
	ldap.getUser(this.email, callback);
})

// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

var autoPopulateInvitees = function(next) {
    this.populate({
        path: 'invitees',
        select: '_id displayName department position invitees',
        options: { sort: { 'displayName': 'asc' } }
    });
    next();
};

var autoPopulateGroups = function(next) {
    this.populate({
        path: 'groups',
        select: '_id name'
    });
    next();
};

var autoPopulateGroupsRequested = function(next) {
    this.populate({
        path: 'groupsRequested',
        select: '_id name'
    });
    next();
};

userSchema
.pre('findOne', autoPopulateInvitees)
.pre('findOne', autoPopulateGroups)
.pre('findOne', autoPopulateGroupsRequested)
.pre('find',    autoPopulateInvitees)

userSchema.methods.hasGroup = function(group) {
    return _.some(this.groups, (el) => el.name === group);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
