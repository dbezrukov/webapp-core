var mongoose = require(__coreModules + '/mongoose');
var Schema = mongoose.Schema;

var schema = Schema({
    email: {
        type: String
    },
    
    /* server-generated email confirmation code */
    emailConfirmationCode: {
        type: String
    },
    
    phone: {
        type: String
    },
    
    /* server-generated phone confirmation code */
    phoneConfirmationCode: {
        type: String
    },
    
    displayName: {
        type: String
    },

    department: {
        type: String
    },

    position: {
        type: String
    },

    /* one who has sent the request */
    inviter: {
        type: Schema.ObjectId, 
        ref: 'User'
    },
    
    /* target group */
    group: {
        type: Schema.ObjectId, 
        ref: 'Group'
    }
});

module.exports = mongoose.model('Invite', schema);