// load the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// define the schema for our user model
var groupSchema = mongoose.Schema({

    name: {
		type: String,
        default: ''
    },
    displayName: {
		type: String,
        default: ''
    }
});

// create the model for users and expose it to our app
module.exports = mongoose.model('Group', groupSchema);
