var config = require('../../config');

var _ = require('underscore');

var User = require('../models/user');

module.exports = function(app, passport) {

    /* Admin views */
    app.get('/admin', isLoggedAdmin, function(req, res) {
        res.render('../public/admin', {
        	title: config.app.name
        });
    });

    app.get('/admin/*', isLoggedAdmin, function(req, res) {
    	res.render('../public/admin', {
			title: config.app.name
    	});
    });

    /* API views */
    app.get('/api', function(req, res) {
        res.render('../public/api', {
        	title: config.app.name
        });
    });

    /* Logout */
    app.get('/logout', function(req, res) {
	    req.logout();
	    res.redirect('/');
	});

	/* Authorization views */
	app.get('/auth', function(req, res) {
    	res.render('../public/auth', {
			title: config.app.name
    	});
    });

    app.get('/auth/*', function(req, res) {
    	res.render('../public/auth', {
			title: config.app.name
    	});
    });

    /* Request views */
	app.get('/request', function(req, res) {
    	res.render('../public/request', {
			title: config.app.name
    	});
    });

    app.get('/request/*', function(req, res) {
    	res.render('../public/request', {
			title: config.app.name
    	});
    });

    /* Main views */
    app.get('/', function(req, res) {

        res.render('../../frontend/main', {
        	title: config.app.name
        });
    });

    app.get('/*', function(req, res) {
        res.render('../../frontend/main', {
        	title: config.app.name
        });
    });
};

function isLoggedAdmin(req, res, next) {
	
    if (!req.isAuthenticated()) {
    	return res.redirect('/auth');
    }

    console.log('check if the user is admin, id: ', req.user.id);

    // get groups information
    User
        .findById(req.user.id)
    	.select({ 
			'groupsAdmin': 1
    	})
    	.populate({
            path: 'groupsAdmin',
            select: '_id name',
            
        })
        .exec(function(err, groupsData) {
        
        	if (err) {
            	res.send(500, { error: 'db error' });
            	return;
        	}

        	var isAdmin = groupsData.groupsAdmin.length > 0;

        	if (isAdmin) {
				console.log('user found in admins');
				return next();

			} else {
				console.log('user not found in admins');
				return res.redirect('/');
			}
		})
}
