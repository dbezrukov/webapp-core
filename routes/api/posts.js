var _ = require('underscore');
var async = require('async');

var Post = require('../../models/post');

module.exports = function(app, passport) {

    app.get('/api/posts', function(req, res) {

        var since = req.query.since ? parseInt(req.query.since) : 0;
        
        var match = {
            'updated': {
                $gte: since
            }
        }

        if (req.query.user) {
            match['user'] = req.query.user;
        }

        if (req.query.root) {
            match['root'] = req.query.root;
        }

        const page = req.query.page ? parseInt(req.query.page) : 1;
        const perPage = req.query.page ? 10 : 100;
        const skip = (page - 1) * perPage;

        Post
            .find(match)
            .limit(perPage)
            .skip(skip)
            .sort({
                updated: req.query.order || -1
            })
            .populate({
                path: 'user',
                select: '_id displayName photo',
            })
            .exec(function(err, posts) {

                if (err) {
                    res.send(500, {
                        error: err
                    });
                    return;
                }

                res.json(posts);
            });
    });

    app.post('/api/posts', function(req, res) {

        async.waterfall([
            function(done) {
                console.log('#create post');

                var data = _.extendOwn(req.body, {
                    user: req.user.id,
                })

                var post = new Post(data);

                post.save(function(err) {
                    done(err, post);
                });
            },
            function(post, done) {
                console.log('#populate user');

                post.populate({
                    path: 'user',
                    select: '_id displayName photo',
                }, function(err) {
                    return done(err, post);
                })
            }
        ], function(err, post) {
            if (err) {
                console.log('#error: ' + err);
                return res.json(500, {
                    error: err
                });
            }

            console.log('#returning new post data');
            res.json(post.toJSON());
        });
    })

    app.get('/api/posts/:id', function(req, res) {

        var postId = req.params.id;
        Post
            .findById(postId)
            .populate({
                path: 'user',
                select: '_id displayName photo',
            })
            .exec(function(err, post) {

                if (err) {
                    res.json(500, {
                        error: err
                    });
                    return;
                }

                if (!post) {
                    res.json(404, {
                        error: 'Пост не найден.'
                    });
                    return;
                }

                res.json(post);
            });
    });

    app.post('/api/posts/:id', function(req, res) {

        var postId = req.params.id;

        Post
            .findById(postId)
            .exec(function(err, post) {
            
            if (err) {
                res.json(500, { error: err });
                return;
            }

            if (!post) {
                res.json(401, { error: 'post not found' });
                return;
            }

            Post
                .findByIdAndUpdate(postId, { $set : req.body }, { new: true })
                .populate({
                    path: 'user',
                    select: '_id displayName photo',
                })
                .exec(function(err, updatedData) {
                
                if (err) {
                    res.json(500, { error: err });
                    return;
                }

                res.json(updatedData.toJSON());
            });

        });
    });

    // Delete the post
    app.delete('/api/posts/:id', function(req, res) {

        var postId = req.params.id;

        Post
            .findByIdAndRemove(postId)
            .exec(function(err, post) {

                if (err) {
                    res.json(500, {
                        error: err
                    });
                    return;
                }

                /*
                if (!app.cloudinary) {
                    res.send(500, {
                        error: 'Cloudinary API not connected.'
                    });
                    return;
                }

                var publicIds = _.pluck(post.files, '_id');
                var postIds = _,filter(publicIds, function(id) {
                    indexOf id
                })

                app.cloudinary.v2.api.delete_resources(postIds, function(result) {
                    res.json({});
                });
                */

                res.json({});
            });
    });
}