var config = require('../../../config');

var async = require('async');
var ssha = require('ssha');

var mailer = require('../../modules/mailer');
var smsSender = require('../../modules/sms-sender');

var User = require('../../models/user');
var Invite = require('../../models/invite');

module.exports = function(app, passport) {

    // creates an invite: email, displayname, department, position, group
    app.post('/api/invites', isLoggedIn, function(req, res) {
        // creates and invite, returns the invite

        async.waterfall([
            // check whether all fields are filled
            function(done) {
                console.log('#check whether all fields are filled');

                function validateEmail(email) {
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(email);
                }

                function validatePhone(phone) {
                    var re = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
                    return re.test(phone);
                }

                if (!validateEmail(req.body.email)) {
                    return done('Пожалуйста, укажите почту.');
                }

                if (!req.body.displayName || req.body.displayName.trim().split(' ').length < 3) {
                    return done('Пожалуйста, укажите Ф.И.О.');
                }

                if (!req.body.department) {
                    return done('Пожалуйста, укажите отдел.');
                }

                if (!req.body.position) {
                    return done('Пожалуйста, укажите должность.');
                }

                done(null);
            },

            // check whether the email is unique
            function(done) {
                console.log('#check whether the email is unique');

                User
                    .findOne({
                        email: req.body.email
                    })
                    .exec(function(err, user) {

                        if (user) {
                            return done('Пользователь с такой почтой уже зарегистрирован.');
                        }

                        done(err);
                    })
            },

            // check whether the same invite is created already
            function(done) {

                console.log('#check whether the same invite is created already');

                Invite
                    .findOne({
                        email: req.body.email
                    })
                    .exec(function(err, invite) {

                        if (invite) {
                            return done('Пользователь с почтой ' + req.body.email + ' уже приглашён.');
                        }

                        done(err);
                    })
            },

            // get inviter
            function(done) {

                console.log('#get inviter');

                User
                    .findById(req.user.id)
                    .exec(function(err, inviter) {
                        if (!inviter) {
                            done('Приглашающий пользователь не найден.');
                            return;
                        }
                        done(err, inviter);
                    })
            },

            // creating invite and sending invitation email
            function(inviter, done) {
                console.log('#creating invite and sending invitation email');

                console.log(req.body);

                var invite = new Invite(req.body);

                invite.inviter = req.user.id;

                mailer.sendInvite(invite.email, invite._id,
                    invite.displayName, inviter.displayName,
                    function(err, emailConfirmationCode) {

                        if (err) {
                            return done('Невозможно создать письмо для приглашения.');
                        }

                        invite.emailConfirmationCode = emailConfirmationCode;
                        invite.save(function(err) {
                            done(err, invite);
                        })
                    })
            },

        ], function(err, invite) {
            if (err) {
                console.log('#error: ' + err);
                return res.json(500, {
                    error: err
                });
            }

            console.log('#returning created invite');
            res.json(invite);
        });

        // returns: invite
    });

    // get invite list
    app.get('/api/invites', isLoggedIn, function(req, res) {

        Invite
            .find({
                inviter: req.user.id
            })
            //.populate('inviter')
            .populate('group')
            .exec(function(err, invites) {

                if (err) {
                    res.json(500, {
                        error: 'db error'
                    });
                    return;
                }

                res.json(invites);
            });

        // returns: invites[]
    });

    // delete an invite
    app.delete('/api/invites/:id', isLoggedIn, function(req, res) {
        var inviteId = req.params.id;

        Invite
            .findByIdAndRemove(inviteId)
            .exec(function(err, invite) {

                if (err) {
                    res.json(500, {
                        error: err
                    });
                    return;
                }

                if (!invite) {
                    res.json(404, {
                        error: 'Not found'
                    });
                    return;
                }

                res.json({});
            });

        // returns: success or 404
    });

    // Public methods ----------------------------------------------------

    // inspect an invite from email + token
    app.get('/api/invites/:id', function(req, res) {
        // user clicks a link from email

        var inviteId = req.params.id;

        Invite
            .findById(inviteId)
            // .populate('inviter')
            .populate('group')
            .exec(function(err, invite) {

                if (err) {
                    res.json(500, {
                        error: err
                    });
                    return;
                }

                if (!invite) {
                    res.json(404, {
                        error: 'Приглашение не найдено.'
                    });
                    return;
                }

                res.json(invite);
            });

        // returns: invite or 404
    });

    // resend sms
    app.post('/api/invites/:id/resend-sms', function(req, res) {

        var inviteId = req.params.id;
        var phone = req.body.phone;

        async.waterfall([

            // check whether all fields are filled
            function(done) {
                console.log('#check whether all fields are filled');

                function validatePhone(phone) {
                    var re = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
                    return re.test(phone);
                }

                if (!validatePhone(phone)) {
                    return done('Неверный телефонный номер.');
                }

                done(null);
            },

            // check whether the same invite is created already
            function(done) {

                console.log('#check whether the same invite is created already');

                Invite
                    .findById(inviteId)
                    .exec(function(err, invite) {
                        if (!invite) {
                            return res.json(404, {
                                error: 'invite not found'
                            });
                        }
                        done(err, invite);
                    })
            },

            // creating invite and sending invitation email
            function(invite, done) {
                console.log('#creating invite and sending sms');

                smsSender.sendConfirmation(phone, function(err, phoneConfirmationCode) {

                    if (err) {
                        return done('Ошибка при отправке СМС, проверьте номер.');
                    }

                    invite.phoneConfirmationCode = phoneConfirmationCode;
                    invite.save(function(err) {
                        done(err, invite);
                    })
                })
            },

        ], function(err, invite) {
            if (err) {
                console.log('#error: ' + err);
                return res.json(500, {
                    error: err
                });
            }

            console.log('#returning created invite');
            res.json(invite);
        });

        // returns: success or error
    });

    // submit an invite (creates a user once all data passed) + token
    app.post('/api/invites/:id/accept', function(req, res) {

        var inviteId = req.params.id;

        var acceptData = req.body;

        console.log('Accepting invite with data');
        console.log(acceptData);

        async.waterfall([
            // find invite by id
            function(done) {
                console.log('#find invite by id');

                Invite
                    .findById(inviteId)
                    .populate('group')
                    .exec(function(err, invite) {
                        if (!invite) {
                            done('Приглашение не найдено.');
                            return;
                        }
                        done(err, invite);
                    })
            },
            // check if the name exists already
            function(invite, done) {
                console.log('#check if the name exists already');

                User
                    .findOne({
                        name: acceptData.name
                    })
                    .exec(function(err, user) {
                        if (user) {
                            done('Пользователь с таким именем уже существует.');
                            return;
                        }
                        done(err, invite);
                    })
            },
            // validate fields
            function(invite, done) {

                if (!acceptData.emailConfirmationCode ||
                    (invite.emailConfirmationCode != acceptData.emailConfirmationCode)) {
                    return done('Неверный код подтверждения email.');
                }

                if (!acceptData.phoneConfirmationCode ||
                    (invite.phoneConfirmationCode != acceptData.phoneConfirmationCode)) {
                    return done('Неверный код SMS-подтверждения.');
                }

                if (!acceptData.password) {
                    return done('Укажите пароль.');
                }

                done(null, invite);
            },
            // create user for this invite
            function(invite, done) {
                console.log('#create user for this invite');

                var reqUnauthorized = req;
                delete reqUnauthorized.user;

                reqUnauthorized.body = {
                    email: invite.email,
                    displayName: invite.displayName,
                    department: invite.department,
                    position: invite.position,

                    name: acceptData.name,
                    password: acceptData.password,
                    phone: acceptData.phone,

                    groups: [invite.group._id],
                    inviter: invite.inviter
                }

                console.log('creating user:');
                console.log(reqUnauthorized.body);

                passport.authenticate('local-signup', function(err, user, info) {
                    if (err) {
                        return done(err);
                    }

                    if (!user) {
                        return done(info.message);
                    }

                    console.log('#local signup done');
                    done(null, invite, user);

                })(reqUnauthorized, res, done);
            },

            // create mattermost user
            function(invite, user, done) {
                console.log('#creating mattermost user');

                if (!app.mattermost) {
                    console.log('mattermost is not configed, skipping');
                    return done(null, invite, user);
                }

                console.log('User full name: ' + user.displayName);

                // Иванов
                var firstname = user.displayName.split(' ')[0];

                // Иван Иванович +79211234567
                var lastname = user.displayName.split(' ')[1] +
                    ' ' + user.displayName.split(' ')[2] +
                    ' ' + user.phone;

                // iivanov
                var username = user.name.split('@')[0];

                var userData = {
                    email: user.name, // with @okocenter.ru
                    username: username,
                    nickname: username,
                    password: acceptData.password,
                    firstname: firstname,
                    lastname: lastname,
                    locale: 'ru'
                }

                app.mattermost.userCreate(userData, function(err) {
                    return done(err, invite, user);
                });
            },

            // add user to mattermost group
            function(invite, user, done) {
                console.log('#adding user to his group');

                if (!app.mattermost) {
                    console.log('mattermost is not configed, skipping');
                    return done(null, invite, user);
                }

                app.mattermost.teamAdd(invite.group.name, user.name, function(err) {
                    done(err, invite, user);
                });
            },

            // add user to LDAP
            function(invite, user, done) {
                console.log('#adding user to his group');

                if (!app.ldap) {
                    console.log('ldap is not configed, skipping');
                    return done(null, invite, user);
                }

                var ldapId = user.name.split('@')[0];

                var hash = ssha.create(acceptData.password);
                app.ldap.addUser(ldapId, user.name, user.displayName, hash, function(err) {
                    done(err, invite, user);
                });
            },

            // delete invite
            function(invite, user, done) {
                console.log('#delete invite');

                Invite
                    .findByIdAndRemove(invite.id)
                    .exec(function(err) {
                        done(err, user);
                    });
            },

            // login
            function(user, done) {
                console.log('#login');

                req.login(user, function(err) {
                    if (err) {
                        return done(err);
                    }

                    var userConfig = app.getUserConfig(user);

                    console.log('Sending user config');
                    console.log(userConfig);

                    done(null, userConfig);
                });
            }

        ], function(err, userConfig) {
            if (err) {
                console.log('#error: ' + err);
                return res.json(500, {
                    error: err
                });
            }

            console.log('#returning user config');
            res.json(userConfig);
        });

        // returns: invite or error
    });
};

function isLoggedIn(req, res, next) {

    if (req.isAuthenticated())
        return next();

    res.send(401, {
        error: 'authorization required'
    });
}