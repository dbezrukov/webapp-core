var config = require('../../../config');

var _ = require('underscore');
var _str = require('underscore.string');
var async = require('async');
var ldap = require('../../modules/ldap');

var Group = require('../../models/group');
var User = require('../../models/user');

module.exports = function(app, passport) {

    app.get('/api/groups', isLoggedIn, function(req, res) {

        async.waterfall([
            // find admin group
            function(done) {
                console.log('#find admin group');

                Group
                    .findOne({
                        name: 'admins'
                    })
                    .exec(done);
            },
            // constructing query
            function(adminGroup, done) {
                console.log('#constructing query');

                if (!adminGroup) {
                    return done('missing admins group');
                }

                var isSuperadmin = _.find(req.user.groupsAdmin, function(groupId) {
                    return groupId.equals(adminGroup._id);
                });

                // superadmin can see all groups except the system Admins one
                var query = {
                    name: {
                        $ne: 'admins'
                    }
                };

                if (!isSuperadmin) {
                    // include all administered groups 
                    query._id = {
                        $in: req.user.groupsAdmin
                    }
                }

                done(null, query);
            },
            // searching
            function(query, done) {
                console.log('#searching groups');

                Group
                    .find(query)
                    .sort({
                        'name': 'asc'
                    })
                    .exec(done);
            }

        ], function(err, groups) {
            if (err) {
                console.log('#error: ' + err);
                return res.json(500, {
                    error: err
                });
            }

            console.log('#returning groups');
            res.json(groups);
        });

    });

    app.post('/api/groups', isLoggedIn, function(req, res) {

        async.waterfall([
            // validate fields
            function(done) {

                if (!req.body.name) {
                    return done('Укажите имя группы.');
                }

                if (!req.body.displayName) {
                    return done('Укажите отображаемое имя группы.');
                }

                done();
            },
            // check whether the group exists already
            function(done) {
                console.log('#check whether the group exists already');

                Group
                    .findOne({
                        name: req.body.name
                    })
                    .exec(function(err, group) {
                        if (group) {
                            return done('Команда с таким именем уже существует.');
                        }
                        done(err);
                    })
            },
            // create group
            function(done) {
                console.log('#create group');

                var group = new Group(req.body);
                done(null, group);
            },
            // add mattermost group
            function(group, done) {
                console.log('#add mattermost group');

                if (!app.mattermost) {
                    console.log('mattermost is not configured, skipping');
                    return done(null, group);
                }

                app.mattermost.teamCreate(group.name, group.displayName, function(err) {
                    done(err, group);
                })
            },
            // add creator to the group
            function(group, done) {
                console.log('#add creator to the group');

                if (!app.mattermost) {
                    console.log('mattermost is not configured, skipping');
                    return done(null, group);
                }

                app.mattermost.teamAdd(group.name, req.user.name, function(err) {
                    done(err, group);
                })
            },
            // save group
            function(group, done) {
                console.log('#saving group');

                group.save(function(err) {
                    done(err, group);
                })
            },
            // save user
            function(group, done) {
                console.log('#saving user');

                req.user.groups.push(group.id);
                req.user.groupsAdmin.push(group.id);

                req.user.save(function(err) {
                    done(err, group);
                })
            },
        ], function(err, group) {
            if (err) {
                console.log('#error: ' + err);
                return res.send(500, {
                    error: err
                });
            }

            console.log('#returning updated request data');
            res.json(group.toJSON());
        });
    });

    app.get('/api/groups/:id', isLoggedIn, function(req, res) {

        var groupId = req.params.id;

        async.waterfall([
            // find admins group
            function(done) {
                console.log('#find admin group');

                Group
                    .findOne({
                        name: 'admins'
                    })
                    .exec(done);
            },
            // check rights
            function(adminGroup, done) {
                console.log('#check rights');

                var isSuperadmin = _.find(req.user.groupsAdmin, function(groupId) {
                    return groupId.equals(adminGroup._id);
                });

                var isAdmin = req.user.groupsAdmin.indexOf(groupId) >= 0;

                if (!isAdmin && !isSuperadmin) {
                    return done('Groups are accessible for admins only.');
                }

                done(null);
            },
            function(done) {

                Group
                    .findById(groupId)
                    .exec(function(err, group) {

                        if (!group) {
                            res.json(404, {
                                error: 'Команда не найдена.'
                            });
                            return;
                        }

                        done(err, group)
                    });

            }
        ], function(err, group) {
            if (err) {
                console.log('#error: ' + err);
                return res.send(500, {
                    error: err
                });
            }

            res.json(group.toJSON());
        });
    });

    app.post('/api/groups/:id', isLoggedIn, function(req, res) {

        var groupId = req.params.id;

        async.waterfall([
            // find admins group
            function(done) {
                console.log('#find admin group');

                Group
                    .findOne({
                        name: 'admins'
                    })
                    .exec(done);
            },
            // check rights
            function(adminGroup, done) {
                console.log('#check rights');

                var isSuperadmin = _.find(req.user.groupsAdmin, function(groupId) {
                    return groupId.equals(adminGroup._id);
                });

                var isAdmin = req.user.groupsAdmin.indexOf(groupId) >= 0;

                if (!isAdmin && !isSuperadmin) {
                    return done('Groups are accessible for admins only.');
                }

                done(null);
            },
            function(done) {

                Group
                    .findByIdAndUpdate(groupId, {
                        $set: req.body
                    }, {
                        new: true
                    })
                    .exec(function(err, group) {

                        if (!group) {
                            res.json(404, {
                                error: 'Команда не найдена.'
                            });
                            return;
                        }

                        done(err, group);
                    });

            }
        ], function(err, group) {
            if (err) {
                console.log('#error: ' + err);
                return res.send(500, {
                    error: err
                });
            }

            res.json(group.toJSON());
        });
    });

    app.delete('/api/groups/:id', isLoggedIn, function(req, res) {

        var groupId = req.params.id;

        async.waterfall([
            // find admin group
            function(done) {
                console.log('#find admin group');

                Group
                    .findOne({
                        name: 'admins'
                    })
                    .exec(done);
            },
            // check rights
            function(adminGroup, done) {
                console.log('#check rights');

                var isSuperadmin = _.find(req.user.groupsAdmin, function(groupId) {
                    return groupId.equals(adminGroup._id);
                });

                var isAdmin = req.user.groupsAdmin.indexOf(groupId) >= 0;

                if (!isAdmin && !isSuperadmin) {
                    return done('Groups are accessible for admins only.');
                }

                done(null);
            },
            // leave this group
            function(done) {
                console.log('#leave this group');

                var query = {
                    $or: [{
                        groups: {
                            $in: [groupId]
                        }
                    }, {
                        groupsAdmin: {
                            $in: [groupId]
                        }
                    }]
                }

                var update = {
                    $pull: {
                        'groups': groupId,
                        'groupsAdmin': groupId
                    }
                }

                User
                    .update(query, update, {
                        multi: true
                    })
                    .exec(function(err, users) {
                        done(err);
                    });
            },
            // remove group
            function(done) {
                console.log('#remove local group');

                Group
                    .findByIdAndRemove(groupId)
                    .exec(function(err, group) {
                        if (!group) {
                            return done('Not found');
                        }

                        done(err, group);
                    });
            },
            // remove mattermost group
            function(group, done) {
                console.log('#remove mattermost group');

                if (!app.mattermost) {
                    console.log('mattermost is not configured, skipping');
                    return done(null);
                }

                app.mattermost.teamDelete(group.name, function(err) {
                    done(err);
                })
            }

        ], function(err) {
            if (err) {
                console.log('#error: ' + err);
                return res.send(500, {
                    error: err
                });
            }

            res.json({});
        });

    });

    /* Group members */

    app.get('/api/groups/:id/members', isLoggedIn, function(req, res) {

        var groupId = req.params.id;

        var query = {
            groups: {
                $in: [groupId]
            }
        };

        User
            .find(query)
            .exec(function(err, users) {

                if (err) {
                    res.send(500, {
                        error: 'db error'
                    });
                    return;
                }

                var members = _.map(users, function(user) {

                    var isAdmin = user.groupsAdmin.indexOf(groupId) >= 0;
                    return {
                        _id: user.id,
                        name: user.name,
                        displayName: user.displayName,
                        isAdmin: isAdmin
                    }
                })

                res.json(members);
            });
    });

    app.post('/api/groups/:id/members', isLoggedIn, function(req, res) {

        var groupId = req.params.id;
        var userId = req.body.userId;

        async.waterfall([
            // get group id
            function(done) {
                console.log('#get group id');

                Group
                    .findById(groupId)
                    .exec(function(err, group) {

                        done(err, group);
                    });
            },
            // get user id
            function(group, done) {
                console.log('#get user id');

                User
                    .findById(userId)
                    .exec(function(err, user) {

                        done(err, group, user);
                    });
            },
            // check if the user is not joined already
            function(group, user, done) {

                if (user.groups.indexOf(groupId) >= 0) {
                    done('Этот пользователь уже участник группы.');
                    return;
                }

                done(null, group, user);
            },
            // join mattermost team
            function(group, user, done) {
                console.log('#join mattermost team');

                if (!app.mattermost) {
                    console.log('mattermost is not configured, skipping');
                    return done(null);
                }

                app.mattermost.teamAdd(group.name, user.name, function(err) {
                    done(err);
                })
            },
            // join the group
            function(done) {
                console.log('#join group');

                var update = {
                    $addToSet: {
                        'groups': groupId
                    }
                }

                User
                    .findByIdAndUpdate(userId, update, {
                        new: true
                    })
                    .exec(function(err, user) {

                        done(err, {
                            _id: user.id,
                            name: user.name,
                            displayName: user.displayName,
                            isAdmin: false
                        })
                    });
            }
        ], function(err, result) {
            if (err) {
                console.log('#error: ' + err);
                return res.send(500, {
                    error: err
                });
            }

            console.log('#returning updated user');
            res.json(result);
        });
    });

    app.delete('/api/groups/:id/members/:userId', isLoggedIn, function(req, res) {

        var groupId = req.params.id;
        var userId = req.params.userId;

        async.waterfall([
            // get group id
            function(done) {
                console.log('#get group id');

                Group
                    .findById(groupId)
                    .exec(function(err, group) {

                        done(err, group);
                    });
            },
            // get user id
            function(group, done) {
                console.log('#get group id');

                User
                    .findById(userId)
                    .exec(function(err, user) {

                        done(err, group, user);
                    });
            },
            // leave mattermost team
            function(group, user, done) {
                console.log('#leave mattermost team');

                if (!app.mattermost) {
                    console.log('mattermost is not configured, skipping');
                    return done(null);
                }

                app.mattermost.teamRemove(group.name, user.name, function(err) {
                    done(err);
                })
            },
            // leave the group
            function(done) {
                console.log('#leave group');

                var update = {
                    $pull: {
                        'groups': groupId,
                        'groupsAdmin': groupId
                    }
                }

                User
                    .findByIdAndUpdate(userId, update, {
                        new: true
                    })
                    .exec(function(err, user) {
                        done(err);
                    });
            },
        ], function(err) {
            if (err) {
                console.log('#error: ' + err);
                return res.send(500, {
                    error: err
                });
            }

            console.log('#returning empty data');
            res.json({});
        });
    });

    app.post('/api/groups/:id/members/:userId', isLoggedIn, function(req, res) {

        var groupId = req.params.id;
        var userId = req.params.userId;
        var isAdmin = req.body.isAdmin;

        async.waterfall([
            // check for self-edit
            function(done) {
                console.log('#check for self-edit');

                if (req.user.id === userId) {
                    return done('Вы не можете менять свои права на доступ.');
                }

                done();
            },

            // update group permissions
            function(done) {
                console.log('#update group permissions');

                var update = isAdmin ?
                    {
                        $addToSet: {
                            'groupsAdmin': groupId
                        }
                    } :
                    {
                        $pull: {
                            'groupsAdmin': groupId
                        }
                    };

                User
                    .findByIdAndUpdate(userId, update, {
                        new: true
                    })
                    .exec(function(err, user) {

                        done(err, {
                            _id: user.id,
                            name: user.name,
                            displayName: user.displayName,
                            isAdmin: isAdmin
                        })
                    });
            }

        ], function(err, user) {
            if (err) {
                console.log('#error: ' + err);
                return res.send(500, {
                    error: err
                });
            }

            console.log('#returning updated user');
            res.json(user);
        });
    });

    /* Group requests */

    app.get('/api/groups/:id/requests', isLoggedIn, function(req, res) {

        var groupId = req.params.id;

        var query = {
            groupsRequested: {
                $in: [groupId]
            }
        };

        User
            .find(query)
            .exec(function(err, users) {

                if (err) {
                    res.send(500, {
                        error: 'db error'
                    });
                    return;
                }

                var requesters = _.map(users, function(user) {

                    return {
                        _id: user.id,
                        name: user.name,
                        displayName: user.displayName,
                    }
                })

                res.json(requesters);
            });
    });

    app.post('/api/groups/:id/requests', isLoggedIn, function(req, res) {

        var groupId = req.params.id;

        async.waterfall([
            // get group id
            function(done) {
                console.log('Group request: check if group exists');

                Group
                    .findById(groupId)
                    .exec(function(err, group) {

                        done(err, group);
                    });
            },
            // check if the user is not joined already
            function(group, done) {

                if (req.user.groupsRequested.indexOf(groupId) >= 0) {
                    console.log('Group request: already sent');
                    done('Этот пользователь уже подал заявление в группу.');
                    return;
                }

                req.user.groupsRequested.push(groupId);

                req.user.save(function(err) {
                    done(err, group);
                })
            }
        ], function(err, result) {
            if (err) {
                console.log('#error: ' + err);
                return res.send(500, {
                    error: err
                });
            }

            console.log('Group request: returning updated user');
            res.json(result);
        });
    });

    app.delete('/api/groups/:id/requests/:userId', isLoggedIn, function(req, res) {

        var groupId = req.params.id;
        var userId = req.params.userId;

        console.log('Deleting moderation request');

        var update = {
            $pull: {
                'groupsRequested': groupId
            }
        }

        User
            .findByIdAndUpdate(userId, update)
            .exec(function(err, user) {

                if (err) {
                    console.log('#error: ' + err);
                    return res.send(500, {
                        error: err
                    });
                }

                res.json({});
            });
    });
};

function isLoggedIn(req, res, next) {

    if (req.isAuthenticated())
        return next();

    res.send(401, {
        error: 'authorization required'
    });
}