var _ = require('underscore');
var config = require('../../../config');
var async = require('async');

var User = require('../../models/user');
var Group = require('../../models/group');

module.exports = function(app, passport) {

    app.get('/api/users', isLoggedIn, function(req, res) {

        async.waterfall([
            // find admin group
            function(done) {
                console.log('#find admin group');

                Group
                    .findOne({
                        name: 'admins'
                    })
                    .exec(function(err, adminGroup) {
                        done(err, adminGroup);
                    });
            },
            // constructing query
            function(adminGroup, done) {
                console.log('#constructing query');

                // superadmin can see all users
                var query = {};

                var exposingUsers = (config.auth && config.auth.exposeUsers);
                if (exposingUsers) {
                    return done(null, query);
                }

                var isAdmin = _.find(req.user.groupsAdmin, function(groupId) {
                    return groupId.equals(adminGroup._id);
                });

                if (!isAdmin) {
                    // include all invited people or belonged to the administered groups
                    query.$or = [{
                            groups: {
                                $in: req.user.groupsAdmin
                            }
                        }, {
                            inviter: req.user.id
                        }]
                        // exlude admins
                    query.groups = {
                        $ne: adminGroup._id
                    }
                }

                done(null, query);
            },
            // searching
            function(query, done) {
                console.log('#searching users');

                User
                    .find(query)
                    .sort({
                        //'displayName': 'asc'
                        '_id': 'asc'
                    })
                    .select({
                        'password': 0,
                        'invitees': 0
                    })
                    .exec(done);
            }

        ], function(err, users) {
            if (err) {
                console.log('#error: ' + err);
                return res.json(500, {
                    error: err
                });
            }

            console.log('#returning users');
            res.json(users);
        });
    });

    app.post('/api/users', function(req, res) {

        console.log('New user:');
        console.log();

        // validate user data
        if (!req.body.groups) {
            return res.json(401, {
                error: 'Не указана группа для пользователя.'
            });
        }

        var unauthorizedReq = req;
        delete unauthorizedReq.user;

        passport.authenticate('local-signup', function(err, user, info) {
            if (err)
                return next(err);

            if (!user) {
                return res.json(401, {
                    error: info.message
                });
            }

            return res.json(user);

        })(unauthorizedReq, res);
    });

    app.get('/api/users/:id', function(req, res) {

        var userId = req.params.id;

        User
            .findById(userId)
            .select({
                'password': 0,
                'invitees': 0
            })
            .populate('inviter')
            .exec(function(err, user) {

                if (err) {
                    res.send(500, {
                        error: 'db error'
                    });
                    return;
                }

                res.json(user);
            });

    });

    app.post('/api/users/:id', function(req, res) {

        var userId = req.params.id;

        var update = {
            $set: req.body
        }

        User
            .findByIdAndUpdate(userId, update, {
                new: true
            })
            .exec(function(err, user) {

                if (err) {
                    console.log('db error: ' + err);
                    res.send(500, {
                        error: 'db error: '
                    });
                    return;
                }

                res.json(user);
            });

    });

    app.delete('/api/users/:id', function(req, res) {

        var userId = req.params.id;

        async.waterfall([
            // search user
            function(done) {
                console.log('#search local user');

                User
                    .findById(userId)
                    .exec(function(err, user) {
                        if (!user) {
                            return done('Not found');
                        }
                        done(err, user);
                    });
            },
            // remove mattermost user
            function(user, done) {
                console.log('#remove mattermost user');

                if (!app.mattermost) {
                    console.log('mattermost is not configured, skipping');
                    return done(null, user);
                }

                // should we remove the user from his team prior to deletion?
                //teamRemove: function(team, email, callback)

                app.mattermost.userDelete(user.name, function(err) {
                    done(err, user);
                })
            },

            // remove ldap user
            function(user, done) {
                console.log('#remove ldap user');

                if (!app.ldap) {
                    console.log('ldap is not configed, skipping');
                    return done(null);
                }

                var ldapId = user.name.split('@')[0];
                app.ldap.removeUser(ldapId, function(err) {
                    done(err);
                });
            },

            // remove user from local database
            function(done) {
                console.log('#remove local user');

                User
                    .findByIdAndRemove(userId)
                    .exec(function(err) {
                        done(err);
                    });
            },


        ], function(err) {
            if (err) {
                console.log('#error: ' + err);
                return res.send(500, {
                    error: err
                });
            }

            res.json({});
        });
    });
};

function isLoggedIn(req, res, next) {

    if (req.isAuthenticated() || (config.auth && config.auth.exposeUsers)) {
        return next();
    }

    res.send(401, {
        error: 'authorization required'
    });
}