var _ = require('underscore');
var async = require('async');
var fs = require('fs');

var Target = require('../../models/target');
var Position = require('../../models/position');

// latest frames for the target media sources
var lastTargetFrames = {};
var sourcesRequested = [];

module.exports = function(app, passport) {

    // quering actual target list
    app.get('/api/targets', function(req, res) {

        var since = req.query.since ? parseInt(req.query.since) : 0;

        var match = {
            'updated': {
                $gte: since
            }
        }

        // include only given type
        if (req.query.type) {
            match['type'] = req.query.type;
        }

        // exlude invalid positions in case quering all records
        if (since === 0) {
            match['valid'] = true;
        }

        Target
            .find(match)
            .sort({
                id: 1
            })
            .select({
                '_id': 0,
                '__v': 0
            })
            .populate({
                path: 'position',
                select: '-_id -__v'
            })
            .exec(function(err, targets) {

                if (err) {
                    res.send(500, {
                        error: err
                    });
                    return;
                }

                res.json({
                    targets: targets,
                    stm: Date.now()
                });
            });
    });

    app.get('/api/targets/registered', function(req, res) {
        Target
            .find({})
            .sort({
                id: 1
            })
            .select({
                'id': 1,
                '_id': 0
            })
            .exec(function(err, targets) {

                if (err) {
                    res.send(500, {
                        error: err
                    });
                    return;
                }

                res.json(targets);
            });
    })

    app.get('/api/targets/sourcesRequested', function(req, res) {

        res.json(sourcesRequested);
    });

    /*
    // quering target page
    app.get('/api/targets/:id', function(req, res) {

        var id = req.params.id;
        var channels = [];

        for (var channel in lastTargetFrames) {
            if (lastTargetFrames.hasOwnProperty(channel)) {
                var channelPath = channel.split('-');

                if (channelPath[0] === id) {
                    channels.push({
                        url: req.protocol + '://' + req.get('host') + req.originalUrl + '/' + channelPath[1],
                        name: channelPath[1]
                    });
                }
            }
        }

        Target
            .findOne({ 'id' : id })
            .select({ 
                'track': 0, 
                '_id': 0,
                '__v': 0,
            })
            .populate({
                 path: 'position',
                 select: '-_id -__v'
            })
            .exec(function(err, data) {
                if (err) {
                    console.log('Error while updating position');
                    res.json({});
                    return;
                } 

                if ( data && data.valid ) {
                    
                    res.render('board', { 
                        board: id,
                        channels: channels.length > 0 ? channels : undefined,
                        online: true
                    });
                    
                } else {

                    res.render('board', { 
                        board: id,
                        channels: channels.length > 0 ? channels : undefined,
                        online: false
                    });

                }
                
            })
    });
    */

    // posting target telemetry
    app.post('/api/targets/:id/telemetry', function(req, res) {

        var id = req.params.id;
        var telemetryData = req.body;

        processTargetTelemetry(id, telemetryData, function(err) {
            if (err) {
                res.send(500, {
                    error: err
                });
                return;
            }

            res.send({});
        });
    });

    // quering target telemetry
    app.get('/api/targets/:id/telemetry', function(req, res) {

        var id = req.params.id;

        Target
            .findOne({
                'id': id
            })
            .select({
                '_id': 0,
                '__v': 0,
            })
            .populate({
                path: 'position',
                select: '-_id -__v'
            })
            .exec(function(err, data) {
                if (err) {
                    res.json({
                        error: err
                    });
                    return;
                }

                if (!data) {
                    res.json(404, {
                        error: 'target not found.'
                    });
                    return;
                }

                res.json(data);

                /*
                if (data && data.valid) {

                    res.json(data);

                } else {

                    res.json({
                        id: id,
                        valid: false,
                    });
                }
                */
            })
    });

    app.get('/api/targets/:id', function(req, res) {

        var id = req.params.id;

        Target
            .findOne({
                'id': id
            })
            .select({
                '_id': 0,
                '__v': 0,
            })
            .populate({
                path: 'position',
                select: '-_id -__v'
            })
            .exec(function(err, data) {
                if (err) {
                    res.json({
                        error: err
                    });
                    return;
                }

                if (!data) {
                    res.json(404, {
                        error: 'target not found.'
                    });
                    return;
                }

                res.json(data);
            })
    });

    app.get('/api/targets/:id/track', function(req, res) {

        var id = req.params.id;

        var timeFrom = req.query.from ? parseInt(req.query.from) : 0;
        var timeTo = req.query.from ? parseInt(req.query.to) : 0;

        Position
            .find({
                'id': id,
                'utc': {
                    $gte: timeFrom,
                    $lte: timeTo
                }
            })
            .select({
                "lat": 1,
                "lon": 1,
                "hdg": 1,
                "hgt": 1,
                "altbaro": 1,
                "speed": 1,
                "data": 1,
                "utc": 1,
                "_id": 0,
            })
            .exec(function(err, track) {
                if (err) {
                    res.send(500, {
                        error: err
                    });
                    return;
                }

                if (!track) {
                    console.log('no track since: ', since);
                    res.json([]);
                    return;
                }

                res.json(track);
            })
    });

    /*
        //var simpleTrack = simplify(track.track, epsilon);

        var simpleTrack = track.track.length > 1 
            ? track.track.slice(0, -1) 
            : track.track;

        //console.log('Track selected: id=', id, ', since=', since, ', length=', 
        //           track.track.length, ', simplified=', simpleTrack.length);

        res.json({ id: id, track: simpleTrack, actualLength: track.track.length });
    */

    app.get('/api/targets/:id/sources', function(req, res) {

        var id = req.params.id;
        var channels = [];

        for (var channel in lastTargetFrames) {
            if (lastTargetFrames.hasOwnProperty(channel)) {

                var channelPath = channel.split('-');

                if (channelPath[0] === id) {
                    channels.push({
                        id: channel,
                        targetId: id,
                        url: '/api/targets/' + id + '/' + channelPath[1] + '/stream?fps=5',
                        name: channelPath[1]
                    });
                }
            }
        }
        
        res.json(channels.sort(function(a, b) {
            return a.name > b.name; // stort by source name: photo, video, wv
        }));
    });

    app.post('/api/targets/:id/:channel', function(req, res) {

        var id = req.params.id;
        var channel = req.params.channel;
        var source = id + '-' + channel;

        var telemetryHeader = req.header('telemetry');
        var telemetryData = telemetryHeader ? JSON.parse(telemetryHeader) : {
            target: id,
            utc: Date.now()
        };
        var frameData = new Buffer('');

        req.on('data', function(chunk) {
            frameData = Buffer.concat([frameData, chunk]);
        });

        req.on('end', function() {
            
            lastTargetFrames[source] = {
                data: frameData,
                telemetry: telemetryData,
                time: Date.now()
            };

            // using video telemetry
            if (telemetryData.type && 
                ((channel.indexOf('video') >= 0 || channel.indexOf('wv') >= 0))) {
                processTargetTelemetry(id, telemetryData, function(err) {
                    res.end();
                });
            } else {
                res.end();
            }
        });
    });

    // quering MJPEG stream directly
    app.get('/api/targets/:id/:channel/stream', function(req, res) {

        var id = req.params.id;
        var channel = req.params.channel;
        var source = id + '-' + channel;

        var fps = req.query.fps ? req.query.fps : 10;

        console.log('Asked stream for board id ' + id);
        console.log('Required FPS is ' + fps);

        if(sourcesRequested.indexOf(source) == -1) {
            sourcesRequested.push(source);
        }

        if (source in lastTargetFrames === false) {
            console.log('no stream for board id ' + id);
            return res.send(404, {
                error: 'no stream for the board'
            });
        }

        console.log('Subscribe to stream from board Id: ' + id);

        var boundary = 'wOOt';

        res.writeHead(200, {
            'Cache-Control': 'no-cache',
            'Pragma': 'no-cache',
            'Expires': 'Thu, 01 Dec 1994 16:00:00 GMT',
            'Connection': 'close',
            'Content-Type': 'multipart/x-mixed-replace; boundary="' + boundary + '"',
        });

        var frameTimer = setInterval(function() {
            var frameData = lastTargetFrames[source].data;

            res.write('--' + boundary + '\r\n');
            res.write('Content-Type: image/jpeg\r\n');

            if (frameData.length > 0) {
                res.write('Content-Length: ' + frameData.length + '\r\n\r\n');
                res.write(frameData);
            } else {

                var frameData = lastTargetFrames['nosignal-video1'].data;
                res.write('Content-Length: ' + frameData.length + '\r\n\r\n');
                res.write(frameData);
            }
            
            res.write('\r\n');
        }, 1000 / fps);

        req.on('close', function() {
            var index = sourcesRequested.indexOf(source);
            if (index >= 0) {
                sourcesRequested.splice(index, 1);
            }
            clearInterval(frameTimer);
        });
    });

    // delete current frame
    /*
    app.get('/api/targets/:id/:channel/delete', function(req, res) {
        
        var id = req.params.id;
        var channel = req.params.channel;
        var source = id + '-' + channel;

        if ((source in lastTargetFrames) === true) {
            delete lastTargetFrames[source];
        }

        res.redirect('/board/' + id + '/' + channel);
    });
    */

    // quering telemetry of the latest frame
    app.get('/api/targets/:id/:channel/telemetry', function(req, res) {

        var id = req.params.id;
        var channel = req.params.channel;
        var source = id + '-' + channel;

        if ((source in lastTargetFrames) === true) {
            res.json(lastTargetFrames[source].telemetry);
        } else {
            res.json({
                board: id,
                utc: Date.now()
            });
        }
    });

    app.get('/api/targets/:id/:channel/status', function(req, res) {

        var id = req.params.id;
        var channel = req.params.channel;
        var source = id + '-' + channel;

        var allow = (sourcesRequested.indexOf(source) >= 0);
        res.writeHead(200, { 'Allow': allow ? '1' : '0' });
        res.end();
    });

    // asking stream page for a board
    /*
    /*
    app.get('/api/targets/:id/:channel', function(req, res) {

        var id = req.params.id;
        var channel = req.params.channel;
        var source = id + '-' + channel;

        console.log('Asked stream page for board id ' + id);

        var streamUrl = req.protocol + '://' + req.get('host') + req.originalUrl + '/stream';
        res.render('video', { 
            channel: channel,
            cargo: channel,
            board: req.params.id,
            streamUrl: streamUrl
        });
    });
    */
}

function processTargetTelemetry(id, telemetryData, callback) {

    // search or create 
    targetById(id, function(err, target) {

        if (err) {
            console.log('error searching target by id: ', err);
            callback(err);
            return;
        }

        var positionData = {
            id: id,
            lat: round(telemetryData.lat, 6),
            lon: round(telemetryData.lon, 6),
            utc: Date.now(), //telemetryData.utc ? telemetryData.utc : Date.now(),
            hgt: round(telemetryData.hgt, 1),
            altbaro: round(telemetryData.altbaro, 1),
            pch: round(telemetryData.pch, 1),
            roll: round(telemetryData.roll, 1),
            yaw: round(telemetryData.yaw, 1),
            hdg: round(telemetryData.hdg, 1),
            speed: round(telemetryData.speed, 0),
            source: telemetryData.source ? telemetryData.source : 'н/д',
            mode: telemetryData.mode ? telemetryData.mode : 'н/д',
            data: {
                speed: round(telemetryData.speed, 0),
                load: 50,
                mode: 'M4'
            }
        }

        var newPosition = new Position(positionData);

        newPosition.save(function(err) {
            if (err) {
                console.log('error processing target position: ', err);
                return callback(err);
            }

            target.valid = true;
            target.type = telemetryData.type;
            target.subtype = telemetryData.subtype || '';
            target.name = telemetryData.name || '';
            target.model = telemetryData.model || '';

            target.updated = newPosition.utc; // actual timestamp or server one

            target.position = newPosition;

            target.save(function(err) {
                return callback(err);
            });
        });

        function round(value, places) {
            if (!value) {
                return 0;
            }
            var newVal = Math.round(value * Math.pow(10, places)) / Math.pow(10, places);
            return parseFloat(newVal);
        }
    });

    function targetById(id, callback) {

        Target.findOne({
            'id': id
        }, function(err, target) {

            if (err)
                return callback(err);

            if (target) {

                return callback(null, target);

            } else {

                if (!telemetryData.type) {
                    return callback('missing target type');
                }

                var newTarget = new Target({
                    type: telemetryData.type
                });

                newTarget.id = id;

                newTarget.save(function(err, createdTarget) {
                    if (err)
                        return callback(err);

                    return callback(null, createdTarget);
                });
            }
        });
    } // eo targetById
}

// mark all invalid locations
setInterval(function() {

    var now = Date.now();
    var locationTimeout = 1 * 60 * 1000; // 1 minute

    var query = {
        'valid': true,
        'updated': {
            $lt: now - locationTimeout
        }
    }

    var update = {
        $set: {
            'valid': false,
            'updated': Date.now()
        }
    }

    Target.update(query, update, {
        multi: true
    }, function(err, result) {
        if (err) {
            console.log('Error while updating online targets: ' + err);
        }
    });

}, 10000);

// delete old locations
setInterval(function() {

    var now = Date.now();
    var sixMonthTimeout = 186 * 24 * 60 * 1 * 60 * 1000;

    var query = {
        'utc': {
            $lt: now - sixMonthTimeout
        }
    }

    Position.remove(query, function(err, result) {
        if (err) {
            console.log('Error while removing old targets: ' + err);
            return;
        }
    });

}, 20 * 60000); // 20 minutes

// delete emulated locations
setInterval(function() {

    var now = Date.now();
    var oneDayTimeout = 24 * 60 * 1* 60 * 1000;

    var query = {
        $and: [
            {
                'utc': {
                    $lt: now - oneDayTimeout
                }
            }, 
            {
                'source': 'emulator'
            }
        ]
    }

    Position.remove(query, function(err, result) {
        if (err) {
            console.log('Error while removing emulated targets: ' + err);
            return;
        }
    });

}, 1 * 60000); // 1 minute

// frame reset timer
setInterval(function() {

    var now = Date.now();

    for (var channel in lastTargetFrames) {
        if (lastTargetFrames.hasOwnProperty(channel)) {

            var elapsed = now - lastTargetFrames[channel].time;
            if (elapsed > 20000) {
                delete lastTargetFrames[channel];
            }
        }
    }

}, 10000);
