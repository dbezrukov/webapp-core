var config = require('../../../config');

var _ = require('underscore');

var User = require('../../models/user');

module.exports = function(app, passport) {

    let socialLogin = [];

    if (config.vk) 
        socialLogin.push('vk');
    if (config.facebook) 
        socialLogin.push('facebook');

    app.get('/api/account/profile', function(req, res) {

        if (!req.isAuthenticated()) {
            console.log('Profile request: user if not authenticated');
            
            return res.json({
                app: config.app.name,
                photostorage: config.app.filestorage ? config.app.filestorage : '',
                socialLogin: socialLogin
            })
        }

        var userId = req.user.id;

        User
            .findById(userId)
            .select({
                'password': 0
            })
            .populate('inviter')
            .exec(function(err, user) {

                if (err) {
                    res.send(500, {
                        error: 'db error'
                    });
                    return;
                }

                var profile = _.extendOwn(user.toJSON(), {
                    app: config.app.name,
                    socialLogin: socialLogin,
                    token: req.cookies[config.session.name],
                    photostorage: config.app.filestorage ? config.app.filestorage : '',
                    cloudinary: config.cloudinary ? config.cloudinary : {},
                    isAdmin: user.groupsAdmin.length > 0,
                    isModerator: !!_.findWhere(user.groups, {
                        name: 'moderators'
                    })
                })

                res.json(profile);
            });
    });

    app.get('/api/account/invitees', isLoggedIn, function(req, res) {

        var userId = req.user.id;

        User
            .findById(userId)
            .select({
                'displayName': 1,
                'department': 1,
                'position': 1,
                'invitees': 1
            })
            .exec(function(err, user) {

                if (err) {
                    res.send(500, {
                        error: 'db error'
                    });
                    return;
                }

                res.json([user]);
            });
    });

    app.post('/api/account')
};

function isLoggedIn(req, res, next) {

    if (req.isAuthenticated())
        return next();

    res.send(401, {
        error: 'authorization required'
    });
}