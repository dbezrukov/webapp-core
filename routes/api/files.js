var _ = require('underscore');
var config = require('../../../config');

// managing files using app.cloudinary

module.exports = function(app, passport) {

    app.delete('/api/files/:id', function(req, res) {

        var publicId = req.params.id;

        if (!app.cloudinary) {
            res.send(500, {
                error: 'Cloudinary API not connected.'
            });
            return;
        }

        app.cloudinary.uploader.destroy(publicId, function(result) {
            res.json(result);
        });
    });

    app.get('/api/files/ocr', function(req, res) {

        var publicId = req.query.file;

        if (!app.cloudinary) {
            res.status(500).json({
                error: 'Cloudinary API not connected.'
            });
            return;
        }

        console.log('extracting text from ' + publicId);

        app.cloudinary.v2.api.update(
            publicId, {
                ocr: 'adv_ocr'
            }, 
            function(error, result) {
                if (error) {
                    console.log(error);
                    res.status(500).json({
                        message: error
                    })
                    return;

                }
                
                res.json({
                    message: result
                }); 
            }
        )
    })

    // TODO: TRY TO EXTRACT TEXT
    /*
    app.get('/api/files/:id/rotate', function(req, res) {

        var publicId = req.params.id;
        
        if (!app.cloudinary) {
            res.send(500, {
                error: 'Cloudinary API not connected.'
            });
            return;
        }

        console.log('rotating image ' + publicId);

        console.log(app.cloudinary.v2.uploader.explicit);

        app.cloudinary.v2.uploader.explicit(
            publicId, {
                type: 'upload',
                eager: [
                    { angle: 90 } 
                ],
                invalidate: true
            }, 
            function(error, result) {
                if (error) {
                    console.log(error);
                    res.status(500).json({
                        message: error
                    })
                    return;

                }
                
                console.log('rotation done with result:');
                console.log(result);

                res.json({
                    message: result
                }); 
            }
        )
    })
    */
}