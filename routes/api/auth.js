var config = require('../../../config');

var _ = require('underscore');
var async = require('async');

var mailer = require('../../modules/mailer');
var smsSender = require('../../modules/sms-sender');

var User = require('../../models/user');
var Invite = require('../../models/invite');

module.exports = function(app, passport) {

    // LOGOUT =====================================================================
    app.post('/api/auth/logout', function(req, res) {

        const isAuthenticated = req.isAuthenticated();
        console.log(`Logout: isAuthenticated: ${isAuthenticated}`);

        if (req.user) {
            console.log('Logging out user:');
            console.log(req.user.displayName);
        }

        req.logout();
        res.end();
    });

    // SIGNUP =====================================================
    app.post('/api/auth/signup/local', function(req, res, next) {

        if (config.app.signupInviteOnly && !req.query.invite) {
            return res.status(401).json({
                error: 'Direct signup is disabled, please request an invitation.'
            });
        }

        if (!req.body.displayName) {
            return res.status(401).json({
                error: 'Пожалуйста, укажите имя и фамилию.'
            });
        }

        if (!req.body.name) {
            return res.status(401).json({
                error: 'Пожалуйста, укажите имя пользователя.'
            });
        }

        if (!validateEmail(req.body.email)) {
            return res.status(401).json({
                error: 'Пожалуйста, укажите email.'
            });
        }

        if (!validatePhone(req.body.phone)) {
            return res.status(401).json({
                error: 'Пожалуйста, укажите телефон в формате +79991112233.'
            });
        }

        console.log(`Checking confirmation code for phone ${req.session.phone}`);
        console.log(`Expected code is ${req.session.phoneConfirmationCode}, provided code is ${req.body.phoneConfirmationCode}`);

        if (req.body.phoneConfirmationCode != req.session.phoneConfirmationCode) {
            return res.status(401).json({
                error: 'Неверный код подтверждения из СМС.'
            });
        }

        if (req.body.phone != req.session.phone) {
            return res.status(401).json({
                error: 'Номер телефона изменен. Пожалуйста, запросите проверочный код снова.'
            });
        }

        const minPasswordLength = config.minPasswordLength || 4;
        if (!req.body.password || req.body.password.length < minPasswordLength ) {
            return res.status(401).json({
                error: 'Пожалуйста, укажите пароль, не менее ' + minPasswordLength + ' символов.'
            });
        }

        passport.authenticate('local-signup', function(err, user, info) {
            if (err) {
                console.log('passport error: ' + err);
                return next(err);
            }

            if (!user) {
                console.log('user not created: ' + info.message);
                return res.status(401).json({
                    error: info.message
                });
            }

            console.log('user authenticated:');
            console.log(user);

            req.login(user, function(err) {
                if (err) {
                    console.log('login error: ' + err);
                    return next(err);
                }

                var userConfig = app.getUserConfig(user);

                console.log('Sending user config');
                console.log(userConfig);

                return res.json(userConfig);
            });

        })(req, res, next);
    });

    // LOGIN =====================================================
    app.post('/api/auth/login/local', function(req, res, next) {

        console.log('loging with username: ' + req.body.username);

        passport.authenticate('local-login', function(err, user, info) {
            if (err) {
                console.log('authenticate error: ' + err);
                return next(err);
            }

            if (!user) {
                console.log('user is not authenticated');
                return res.status(401).json({
                    error: info.message
                });
            }

            req.login(user, function(err) {
                if (err) {
                    return next(err);
                }

                var userConfig = app.getUserConfig(user);

                console.log('Sending user config');
                console.dir(userConfig);

                return res.json(userConfig);
            });

        })(req, res, next);
    });

    // FORGOT PASSWORD ============================================================

    app.post('/api/auth/forgot', function(req, res, next) {

        async.waterfall([

            // check email exists
            function(done) {
                console.log('#check email exists');

                if (!validateEmail(req.body.email)) {
                    return done('Введен неверный email.');
                }

                done();
            },
            // find user by email
            function(done) {
                console.log('#find user by email');

                User
                    .findOne({
                        email: req.body.email
                    })
                    .exec(function(err, user) {
                        if (err) {
                            return done(err);
                        }

                        // user with given email is found
                        if (user) {
                            return done(null, user);
                        }

                        // try to search within emails given in registration reques
                        Invite
                            .findOne({
                                email: req.body.email
                            })
                            .exec(function(err, invite) {
                                if (err) {
                                    return done(err);
                                }

                                if (!invite) {
                                    return done('Пользователь с такой почтой не найден.');
                                }

                                User
                                    .findById(invite.userId)
                                    .exec(function(err, user) {

                                        console.log('changing password for ' + user.name);
                                        return done(err, user);
                                    });
                            });
                    })
            },
            // generate signup token
            function(user, done) {
                console.log('#generate signup token');

                require('crypto').randomBytes(20, function(err, buf) {
                    var token = buf.toString('hex');
                    done(err, user, token);
                })
            },
            // save user
            function(user, token, done) {
                console.log('#save user');

                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000 * 24 * 365; // 1 year

                user.save(function(err) {
                    done(err, user);
                })
            },
            // send email
            function(user, done) {
                console.log('#send email');

                mailer.sendPasswordReset(req.body.email, user.resetPasswordToken, function(err) {
                    done(err, user);
                });
            }

        ], function(err, user) {
            if (err) {
                console.log('#error: ' + err);
                return res.status(500).json({
                    error: err
                });
            }

            console.log('#returning');
            res.json({});
        });
    });

    // RESET PASSWORD =============================================================

    app.post('/api/auth/reset', function(req, res, next) {

        async.waterfall([

            // find and update a user for the given token
            function(done) {

                console.log('#find and update a user for the given token');


                User.findOne({
                    resetPasswordToken: req.body.token,
                    resetPasswordExpires: {
                        $gt: Date.now()
                    }

                }, function(err, user) {

                    if (!user) {
                        return done('Ссылка для сброса пароля использована или просрочена.');
                    }

                    user.password = user.generateHash(req.body.password);
                    user.resetPasswordToken = undefined;
                    user.resetPasswordExpires = undefined;

                    user.save(function(err) {
                        req.logIn(user, function(err) {
                            done(err, user, req.body.password);
                        });
                    });
                });
            },
            // update mattermost password
            function(user, password, done) {

                console.log('#update mattermost password');

                if (!app.mattermost) {
                    console.log('mattermost is not configed, skipping');
                    return done(null);
                }

                app.mattermost.userPassword(user.name, password, done);
            },
            // send email that password has been changed
            function(done) {

                console.log('#send email that password has been changed');

                done();
            }
        ], function(err) {

            if (err) {
                return res.status(500).json({
                    error: err
                });
            }

            console.log('#returning success');

            res.json({});
        });
    });

    app.get('/api/auth/vk', function(req, res, next) {
        req.session.return_to = req.query.return_to;
        req.session.deviceId = req.query.deviceId;
        passport.authenticate('vkontakte', { scope: ['email'] })(req, res, next);
    });

    app.get('/api/auth/vk/callback',
        passport.authenticate('vkontakte', { failureRedirect: '/api/auth/vk' }),
        (req, res) => performRedirect(req, res)
    );

    app.get('/api/auth/facebook', function(req, res, next) {
        req.session.return_to = req.query.return_to;
        req.session.deviceId = req.query.deviceId;
        passport.authenticate('facebook', { scope: ['public_profile', 'email'] })(req, res, next);
    });

    app.get('/api/auth/facebook/callback',
        passport.authenticate('facebook', { failureRedirect: '/api/auth/facebook' }),
        (req, res) => performRedirect(req, res)
    );

    app.post('/api/auth/resend-sms', function(req, res) {

        var phone = req.body.phone;

        async.waterfall([

            // check whether all fields are filled
            function(done) {
                console.log('#check whether all fields are filled');

                if (!validatePhone(phone)) {
                    return done('Неверный телефонный номер.');
                }

                done(null);
            },

            // sending sms
            function(done) {
                console.log('#sending sms');

                smsSender.sendConfirmation(phone, function(err, phoneConfirmationCode) {

                    if (err) {
                        return done('Ошибка при отправке СМС, проверьте номер.');
                    }

                    req.session.phone = phone;
                    req.session.phoneConfirmationCode = phoneConfirmationCode;
                    
                    done(err);
                })
            },

        ], function(err) {
            if (err) {
                console.log('#error: ' + err);
                return res.status(500).json({
                    error: err
                });
            }

            res.json({});
        });
    });
};

function performRedirect(req, res) {
    var redirect_uri = req.session.return_to || '/';
            
    // provide user info for mobile client
    if (redirect_uri.indexOf('auth.expo.io') >= 0) {

        console.log(`Auth: redirecting back to the app, token: ${req.cookies[config.session.name]}`);

        var user = {
            _id: req.user.id,
            name: req.user.name,
            displayName: req.user.displayName,
            groups: req.user.groups,
            groupsRequested: req.user.groupsRequested,
            photo: req.user.photo,
            photostorage: config.app.filestorage ? config.app.filestorage : '',
            token: req.cookies[config.session.name]
        }

        redirect_uri += '?user=' + encodeURIComponent(JSON.stringify(user));
    }
    res.redirect(redirect_uri);
}

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhone(phone) {
    var re = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
    return re.test(phone);
}

